-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.34 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.3.0.6589
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping database structure for qlypkdongy
DROP DATABASE IF EXISTS `qlypkdongy`;
CREATE DATABASE IF NOT EXISTS `qlypkdongy` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `qlypkdongy`;

-- Dumping structure for table qlypkdongy.admin
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `password` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.admin: ~0 rows (approximately)
# DELETE FROM `admin`;
INSERT INTO `admin` (`username`, `password`) VALUES ('admin', 'admin123');

# -- Dumping structure for table qlypkdongy.advice
DROP TABLE IF EXISTS `advice`;
CREATE TABLE IF NOT EXISTS `advice` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_prescription` int DEFAULT NULL,
  `description` varchar(10000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_prescription`  (`id_prescription`),

  CONSTRAINT `advice_ibfk_1` FOREIGN KEY (`id_prescription`) REFERENCES `prescription` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.advice: ~0 rows (approximately)
# DELETE FROM `advice`;

-- Dumping structure for table qlypkdongy.clinic
DROP TABLE IF EXISTS `clinic`;
CREATE TABLE IF NOT EXISTS `clinic` (
  `id_clinic` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `description` varchar(10000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `representatives` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_clinic`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.clinic: ~0 rows (approximately)
# DELETE FROM `clinic`;

-- Dumping structure for table qlypkdongy.diagnosis
DROP TABLE IF EXISTS `diagnosis`;
CREATE TABLE IF NOT EXISTS `diagnosis` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_me` int DEFAULT NULL,
  `disease` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `medical_information` varchar(10000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `diagnosis_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_me` (`id_me`),
  CONSTRAINT `diagnosis_ibfk_1` FOREIGN KEY (`id_me`) REFERENCES `medical_examination` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.diagnosis: ~0 rows (approximately)
# DELETE FROM `diagnosis`;

-- Dumping structure for table qlypkdongy.doctor
DROP TABLE IF EXISTS `doctor`;
CREATE TABLE IF NOT EXISTS `doctor` (
  `id_doctor` int NOT NULL AUTO_INCREMENT,
  `name_doctor` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `gender` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `description_doctor` varchar(10000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `title` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `academic_rank` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `degree` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `avatar_path` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `username` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `password` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_doctor`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.doctor: ~0 rows (approximately)
# DELETE FROM `doctor`;
INSERT INTO `doctor` (`name_doctor`, `gender`, `address`, `description_doctor`, `title`, `academic_rank`, `degree`, `phone`, `avatar_path`, `username`, `password`, `status`, `create_date`, `update_date`) 
VALUES 
('Lê Quang Minh', 'Male', 'Address 1', 'Bác sĩ Minh là một người đã tận tâm cống hiến với ngành nhi', 'Dr.', 'Professor', 'MD', '1234567891', '/img/nam1.jpg', 'doctor1', 'password1', 1, NOW(), NOW()),
('Hoàng Thị Hà', 'Female', 'Address 2', 'Description for Doctor 2', 'Dr.', 'Associate Professor', 'MD', '1234567892', '/img/nu1.jpg', 'doctor2', 'password2', 1, NOW(), NOW()),
('Lê Văn Lương', 'Male', 'Address 3', 'Description for Doctor 3', 'Dr.', 'Assistant Professor', 'MD', '1234567893', '/img/nam2.jpg', 'doctor3', 'password3', 1, NOW(), NOW()),
('Đặng Thi Xuân', 'Female', 'Address 4', 'Description for Doctor 4', 'Dr.', 'Professor', 'MD', '1234567894', '/img/nu2.jpg', 'doctor4', 'password4', 1, NOW(), NOW()),
('Doctor 5 Name', 'Male', 'Address 5', 'Description for Doctor 5', 'Dr.', 'Associate Professor', 'MD', '1234567895', '/img/Avatar.png', 'doctor5', 'password5', 1, NOW(), NOW()),
('Doctor 6 Name', 'Female', 'Address 6', 'Description for Doctor 6', 'Dr.', 'Assistant Professor', 'MD', '1234567896', '/img/Avatar.png', 'doctor6', 'password6', 1, NOW(), NOW()),
('Doctor 7 Name', 'Male', 'Address 7', 'Description for Doctor 7', 'Dr.', 'Professor', 'MD', '1234567897', '/img/Avatar.png', 'doctor7', 'password7', 1, NOW(), NOW()),
('Doctor 8 Name', 'Female', 'Address 8', 'Description for Doctor 8', 'Dr.', 'Associate Professor', 'MD', '1234567898', '/img/Avatar.png', 'doctor8', 'password8', 1, NOW(), NOW()),
('Doctor 9 Name', 'Male', 'Address 9', 'Description for Doctor 9', 'Dr.', 'Assistant Professor', 'MD', '1234567899', '/img/Avatar.png', 'doctor9', 'password9', 1, NOW(), NOW()),
('Doctor 10 Name', 'Female', 'Address 10', 'Description for Doctor 10', 'Dr.', 'Professor', 'MD', '1234567800', '/img/Avatar.png', 'doctor10', 'password10', 1, NOW(), NOW());

-- Dumping structure for table qlypkdongy.health_index
DROP TABLE IF EXISTS `health_index`;
CREATE TABLE IF NOT EXISTS `health_index` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_me` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `weight` int DEFAULT NULL,
  `heart_rate` int DEFAULT NULL,
  `blood_pressure` int DEFAULT NULL,
  `other_info` varchar(10000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `examination_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_me` (`id_me`),
  CONSTRAINT `health_index_ibfk_1` FOREIGN KEY (`id_me`) REFERENCES `medical_examination` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.health_index: ~0 rows (approximately)
# DELETE FROM `health_index`;

-- Dumping structure for table qlypkdongy.item
DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `id_item` int NOT NULL AUTO_INCREMENT,
  `name_item` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `description` varchar(10000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.item: ~0 rows (approximately)
# DELETE FROM `item`;
INSERT INTO `item` (`id_item`, `name_item`, `description`, `type`, `price`, `unit`, `status`, `create_date`, `update_date`) VALUES
	(50, 'Dương quy', 'Dùng để cải thiện hệ thống tuần hoàn máu, giảm đau và chống viêm.', 'Thuốc', 26000, 'Hộp', 1, '2023-11-01 10:59:26', '2023-11-01 10:59:26'),
	(51, 'Nhân sâm', 'Tăng sức đề kháng, giúp giảm mệt mỏi và tăng cường năng lượng.', 'Thuốc', 54000, 'Gói', 1, '2023-11-01 10:59:26', '2023-11-01 10:59:26'),
	(52, 'Hạt sen', 'Chứa nhiều chất xơ, giúp tiêu hóa tốt và giảm cân.', 'Thuốc', 92000, 'Gói', 1, '2023-11-01 10:59:26', '2023-11-01 10:59:26'),
	(53, 'Cam thảo', 'Có tính chất chống viêm, giúp làm dịu đau họng và hỗ trợ tiêu hóa.', 'Thuốc', 98000, 'Gói', 1, '2023-11-01 10:59:26', '2023-11-01 10:59:26'),
	(54, 'Lúa mạch', 'Lúa mạch nguyên chất giàu chất xơ, giúp tăng cường sức khỏe tim mạch và giảm cholesterol.', 'Thuốc', 14000, 'Bao', 1, '2023-11-01 10:59:26', '2023-11-01 10:59:26'),
	(55, 'Kim ngân hoa', 'Giúp giảm căng thẳng, làm dịu tinh thần và cải thiện giấc ngủ.', 'Thuốc', 76000, 'Hộp', 1, '2023-11-01 10:59:26', '2023-11-01 10:59:26'),
	(56, 'Mật ong', 'Mật ong tự nhiên giúp cải thiện hệ thống miễn dịch và có tính chất chống viêm.', 'Vật tư', 40000, 'Chai', 1, '2023-11-01 10:59:26', '2023-11-01 10:59:26'),
	(57, 'Gừng', 'Gừng có tính năng giúp giảm buồn nôn và đau rát cơ thể.', 'Thuốc', 71000, 'Gói', 1, '2023-11-01 10:59:26', '2023-11-01 10:59:26'),
	(58, 'Cúc chương', 'Cúc chương giúp giảm căng thẳng và cải thiện tinh thần.', 'Thuốc', 34000, 'Hộp', 1, '2023-11-01 10:59:26', '2023-11-01 10:59:26'),
	(59, 'Cao hoa hòe', 'Dùng để chữa các vấn đề về tiêu hóa và giúp giảm viêm.', 'Thuốc', 60000, 'Hộp', 1, '2023-11-01 10:59:26', '2023-11-01 10:59:26'),
	(70, 'Băng cứu thương', 'Băng cứu thương là vật liệu y tế dùng để bọc vết thương, giữ cho vùng thương tổn ổn định và sạch sẽ, ngăn chặn vi khuẩn và hỗ trợ quá trình lành mạnh của vết thương.', 'Vật tư', 30000, 'Hộp', 1, '2023-11-01 11:01:34', '2023-11-01 11:01:35');

-- Dumping structure for table qlypkdongy.item_import
# DROP TABLE IF EXISTS `item_import`;
CREATE TABLE IF NOT EXISTS `item_import` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_provider` int DEFAULT NULL,
  `id_item` int DEFAULT NULL,
  `import_price` float DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `total_price` float DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `import_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_provider` (`id_provider`),
  KEY `id_item` (`id_item`),
  CONSTRAINT `item_import_ibfk_1` FOREIGN KEY (`id_provider`) REFERENCES `provider` (`id_provider`),
  CONSTRAINT `item_import_ibfk_2` FOREIGN KEY (`id_item`) REFERENCES `item` (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.item_import: ~0 rows (approximately)
# DELETE FROM `item_import`;
INSERT INTO `item_import` (`id`, `id_provider`, `id_item`, `import_price`, `quantity`, `total_price`, `status`, `import_date`, `update_date`) VALUES
	(1, 1, 70, 15000, 50, 750000, 1, '2023-11-01 11:08:53', '2023-11-01 11:08:53');

-- Dumping structure for table qlypkdongy.medical_examination
DROP TABLE IF EXISTS `medical_examination`;
CREATE TABLE IF NOT EXISTS `medical_examination` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_patient` int DEFAULT NULL,
  `id_doctor` int DEFAULT NULL,
  `reason_to_visit` varchar(10000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `reception_time` datetime DEFAULT NULL,
  `appointment_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_patient` (`id_patient`),
  KEY `id_doctor` (`id_doctor`),
  CONSTRAINT `medical_examination_ibfk_1` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`id_patient`),
  CONSTRAINT `medical_examination_ibfk_2` FOREIGN KEY (`id_doctor`) REFERENCES `doctor` (`id_doctor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.medical_examination: ~0 rows (approximately)
# DELETE FROM `medical_examination`;

-- Dumping structure for table qlypkdongy.patient
DROP TABLE IF EXISTS `patient`;
CREATE TABLE IF NOT EXISTS `patient` (
  `id_patient` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `gender` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `job` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `country` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `identification_code` varchar(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_patient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.patient: ~0 rows (approximately)
# DELETE FROM `patient`;

-- Dumping structure for table qlypkdongy.prescription
DROP TABLE IF EXISTS `prescription`;
CREATE TABLE IF NOT EXISTS `prescription` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_me` int DEFAULT NULL,
  `id_item` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `total_price` float DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_item` (`id_item`),
  KEY `id_me` (`id_me`),
  CONSTRAINT `prescription_ibfk_1` FOREIGN KEY (`id_item`) REFERENCES `item` (`id_item`),
  CONSTRAINT `prescription_ibfk_2` FOREIGN KEY (`id_me`) REFERENCES `medical_examination` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.prescription: ~0 rows (approximately)
# DELETE FROM `prescription`;

-- Dumping structure for table qlypkdongy.provider
DROP TABLE IF EXISTS `provider`;
CREATE TABLE IF NOT EXISTS `provider` (
  `id_provider` int NOT NULL AUTO_INCREMENT,
  `name_provider` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `representatives` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `description` varchar(10000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_provider`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table qlypkdongy.provider: ~0 rows (approximately)
# DELETE FROM `provider`;
INSERT INTO `provider` (`id_provider`, `name_provider`, `address`, `phone`, `representatives`, `description`, `status`, `create_date`, `update_date`) VALUES
	(1, 'Công Ty TNHH thuốc đông y Thành Long', 'Số 43 đường Lý Tự Trọng', '0988776655', 'Ông Nguyễn Thành Long', 'Công ty chuyên cung cấp thuốc, dươc liệu đông y', 1, '2023-11-01 11:04:28', '2023-11-01 11:04:29');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
