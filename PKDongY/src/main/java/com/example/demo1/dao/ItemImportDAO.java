package com.example.demo1.dao;

import com.example.demo1.model.ItemImport;
import com.ibm.icu.text.Transliterator;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ItemImportDAO extends DBContext {

    private static final String GET_ALL_ITEM_IMPORT = "SELECT * FROM item_import ORDER BY id DESC";

    private static final String SEARCH_IMPORT = "SELECT item_import.id\n" +
            "FROM item_import \n" +
            "INNER JOIN item ON item_import.id_item = item.id_item\n" +
            "INNER JOIN provider ON provider.id_provider = item_import.id_provider\n" +
            "WHERE item.name_item LIKE ? OR provider.name_provider LIKE ? ORDER BY id DESC";
    private static final String UPDATE_IMPORT = "UPDATE item_import SET id_provider = ?, id_item = ?, import_price = ?, quantity = ?, total_price = ?, status = ?, update_date = NOW() WHERE id = ?;";
    private static final String ADD_IMPORT = "INSERT INTO item_import(id_provider, id_item,import_price,quantity, total_price,`status`,import_date,update_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";
    private static final String DELETE_IMPORT = "DELETE FROM item_import WHERE id = ?";

    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
        ItemImportDAO adao = new ItemImportDAO();
        ArrayList<ItemImport> al = adao.getAll();
        System.out.println(al.get(0).getTotalPrice());
    }

    public ArrayList<ItemImport> getAll() {
        ArrayList<ItemImport> listII = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_ITEM_IMPORT);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listII.add(new ItemImport(
                        rs.getInt(1), rs.getInt(2),
                        rs.getInt(3), rs.getFloat(4),
                        rs.getInt(5), rs.getFloat(6),
                        rs.getInt(7), rs.getString(8),
                        rs.getString(9)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllItemImport: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemImportDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return listII;
    }

    public static String removeAccents(String input) {
        Transliterator transliterator = Transliterator.getInstance("NFD; [:Nonspacing Mark:] Remove; NFC;");
        return transliterator.transform(input);
    }

    public ArrayList<ItemImport> searchImport(String searchKey) {
        String normalizedSearchKey = removeAccents(searchKey);
        ArrayList<ItemImport> itemImports = new ArrayList<>();
        ItemImportDAO itemImportDAO = new ItemImportDAO();
        ArrayList<ItemImport> listA = itemImportDAO.getAll();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(SEARCH_IMPORT);
            pstm.setString(1, "%" + searchKey + "%");
            pstm.setString(2, "%" + searchKey + "%");
            rs = pstm.executeQuery();
            while (rs.next()) {
                for (ItemImport itim : listA) {
                    if (itim.getId() == rs.getInt(1)) {
                        itemImports.add(itim);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("getAllItemImport: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemImportDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return itemImports;
    }

    public ItemImport getItemImportById(int id) {
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_ITEM_IMPORT);
            rs = pstm.executeQuery();
            if (rs.next()) {
                return new ItemImport(
                        rs.getInt(1), rs.getInt(2),
                        rs.getInt(3), rs.getFloat(4),
                        rs.getInt(5), rs.getFloat(6),
                        rs.getInt(7), rs.getString(8),
                        rs.getString(9)
                );
            }
        } catch (Exception e) {
            System.out.println("getAllItemImport: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemImportDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return null;
    }

    public int updateImport(ItemImport itemImport) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = localDateTime.format(formatter);
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(UPDATE_IMPORT);
            pstm.setInt(1, itemImport.getIdProvider());
            pstm.setInt(2, itemImport.getIdItem());
            pstm.setFloat(3, itemImport.getImportPrice());
            pstm.setInt(4, itemImport.getQuantity());
            pstm.setFloat(5, itemImport.getTotalPrice());
            pstm.setInt(6, itemImport.getStatus());

            pstm.setInt(7, itemImport.getId());
            pstm.executeUpdate();
            return 1;
        } catch (Exception e) {
            System.out.println("updateImport: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemImportDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return 0;
    }

    public int addImport(ItemImport itemImport) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = localDateTime.format(formatter);
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(ADD_IMPORT);
            pstm.setInt(1, itemImport.getIdProvider());
            pstm.setInt(2, itemImport.getIdItem());
            pstm.setFloat(3, itemImport.getImportPrice());
            pstm.setInt(4, itemImport.getQuantity());
            pstm.setFloat(5, itemImport.getTotalPrice());
            pstm.setInt(6, 1);
            pstm.setString(7, formattedDateTime);
            pstm.setString(8, formattedDateTime);
            pstm.executeUpdate();
            return 1;
        } catch (Exception e) {
            System.out.println("AddImport: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemImportDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return 0;
    }

    public int deleteImport(int id) {
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(DELETE_IMPORT);
            pstm.setInt(1, id);
            pstm.executeUpdate();
            return 1;
        } catch (Exception e) {
            System.out.println("AddImport: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemImportDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return 0;
    }
}
