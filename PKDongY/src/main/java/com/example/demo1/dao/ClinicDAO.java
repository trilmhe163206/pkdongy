package com.example.demo1.dao;

import com.example.demo1.model.Clinic;
import com.example.demo1.model.Doctor;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClinicDAO extends DBContext {

    private static final String GET_ALL_CLINIC = "SELECT * FROM clinic";

    private static final String GET_CLINIC_BY_ID = "SELECT * FROM clinic where id_clinic = ?";

    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
        ClinicDAO adao = new ClinicDAO();
        ArrayList<Clinic> al = adao.getAll();
        System.out.println(al);
    }

    public ArrayList<Clinic> getAll() {
        ArrayList<Clinic> listD = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_CLINIC);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listD.add(new Clinic(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getString(9)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllClinic: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ClinicDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return listD;
    }

    public Clinic getClinicById(int id) {
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_CLINIC_BY_ID);
            pstm.setInt(1, id);
            rs = pstm.executeQuery();
            if (rs.next()) {
                return new Clinic(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getString(9)
                );
            }
        } catch (Exception e) {
            System.out.println("getClinicById: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ClinicDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return null;
    }
}
