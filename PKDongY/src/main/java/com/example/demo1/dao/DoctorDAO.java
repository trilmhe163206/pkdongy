package com.example.demo1.dao;

import com.example.demo1.model.Account;
import com.example.demo1.model.Clinic;
import com.example.demo1.model.Doctor;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DoctorDAO extends DBContext {

    private static final String GET_DOCTOR_BY_LOGIN = "SELECT * FROM doctor WHERE username = ? and password  = ?";

    private static final String SEARCH_DOCTOR = "SELECT * FROM doctor WHERE name_doctor LIKE ? OR phone LIKE ? OR username LIKE ?;";

    Connection connect = null;
    Statement stm;
    PreparedStatement pstm;
    ResultSet rs;

    public ArrayList<Doctor> getAllDoctor() {
        ArrayList<Doctor> listA = new ArrayList<>();
        String strSelect = "select * from doctor";
        try {
            connect = DBContext.getConnection();
            pstm = connect.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listA.add(new Doctor(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10),
                        rs.getString(11), rs.getString(12),
                        rs.getBoolean(13),
                        rs.getString(14), rs.getString(15)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllCourse: " + e.getMessage());
        } finally {
            try {
                connect.close();
            } catch (SQLException e) {
                Logger.getLogger(DoctorDAO.class.getName()).log(Level.SEVERE, null, e);
            }
            return listA;
        }
    }

    public Doctor getInfoByLogin(Account account) {
        Doctor d = null;
        try {
            pstm = conn.prepareStatement(GET_DOCTOR_BY_LOGIN);
            pstm.setString(1, account.getUsername());
            pstm.setString(2, account.getPassword());
            rs = pstm.executeQuery();
            while (rs.next()) {
                d = new Doctor(rs.getInt(1), rs.getString(2),
                        rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10),
                        rs.getString(11), rs.getString(12),
                        rs.getBoolean(13), rs.getString(14),
                        rs.getString(15)
                );
            }
        } catch (Exception e) {
            System.out.println("getInfoByLogin: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(DoctorDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return d;
    }

    public static void main(String[] args) {
        DoctorDAO adao = new DoctorDAO();
        Doctor al = adao.getDoctorByID("1");
        System.out.println(al);
    }

    public ArrayList<Doctor> searchDoctor(String search) {
        ArrayList<Doctor> listA = new ArrayList<>();
        try {
            String strSelect = SEARCH_DOCTOR;
            connect = DBContext.getConnection();
            pstm = connect.prepareStatement(strSelect);
            pstm.setString(1, "%" + search + "%");
            pstm.setString(2, "%" + search + "%");
            pstm.setString(3, "%" + search + "%");
            rs = pstm.executeQuery();
            while (rs.next()) {
                listA.add(new Doctor(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10),
                        rs.getString(11), rs.getString(12),
                        rs.getBoolean(13),
                        rs.getString(14), rs.getString(15)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllCourse: " + e.getMessage());
        } finally {
            try {
                connect.close();
            } catch (SQLException e) {
                Logger.getLogger(DoctorDAO.class.getName()).log(Level.SEVERE, null, e);
            }
            return listA;
        }
    }

    public ArrayList<Doctor> searchDoctorAndStatus(String search, String status) {
        ArrayList<Doctor> listA = new ArrayList<>();
        try {
            String strSelect = "SELECT * FROM doctor\n" +
                    "WHERE status = ?\n" +
                    "AND (name_doctor LIKE ? OR phone LIKE ? OR username LIKE ?);\n";
            connect = DBContext.getConnection();
            pstm = connect.prepareStatement(strSelect);
            pstm.setString(1, status);
            pstm.setString(2, "%" + search + "%");
            pstm.setString(3, "%" + search + "%");
            pstm.setString(4, "%" + search + "%");
            rs = pstm.executeQuery();
            while (rs.next()) {
                listA.add(new Doctor(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10),
                        rs.getString(11), rs.getString(12),
                        rs.getBoolean(13),
                        rs.getString(14), rs.getString(15)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllCourse: " + e.getMessage());
        } finally {
            try {
                connect.close();
            } catch (SQLException e) {
                Logger.getLogger(DoctorDAO.class.getName()).log(Level.SEVERE, null, e);
            }
            return listA;
        }

    }

    public ArrayList<Doctor> searchStatus(String status) {
        ArrayList<Doctor> listA = new ArrayList<>();
        try {
            String strSelect = "SELECT * FROM doctor\n" +
                    "WHERE  status = ?";
            connect = DBContext.getConnection();
            pstm = connect.prepareStatement(strSelect);
            pstm.setString(1, status);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listA.add(new Doctor(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10),
                        rs.getString(11), rs.getString(12),
                        rs.getBoolean(13),
                        rs.getString(14), rs.getString(15)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllCourse: " + e.getMessage());
        } finally {
            try {
                connect.close();
            } catch (SQLException e) {
                Logger.getLogger(DoctorDAO.class.getName()).log(Level.SEVERE, null, e);
            }
            return listA;
        }
    }

    public void insertDoctorToDB(Doctor doctor) {
        String sql = "INSERT INTO `doctor` (`name_doctor`, `address`, `description_doctor`, `title`, `academic_rank`, `degree`, `phone`, `avatar_path`, `username`, `password`, `status`, `create_date`, `update_date`) \n" +
                "VALUES (?,?,?,?,?,?,?,?,?,?,1, NOW(), NOW());";
        try {

            connect = DBContext.getConnection();
            PreparedStatement pstm = connect.prepareStatement(sql);
            pstm.setString(1, doctor.getNameDoctor());
            pstm.setString(2, doctor.getAddress());
            pstm.setString(3, doctor.getDescriptionDoctor());
            pstm.setString(4, doctor.getTitleDoctor());
            pstm.setString(5, doctor.getAcademicRank());
            pstm.setString(6, doctor.getDegree());
            pstm.setString(7, doctor.getPhone());
            pstm.setString(8, doctor.getAvatar_path());
            pstm.setString(9, doctor.getUsername());
            pstm.setString(10, doctor.getPassword());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println("insertdoctor: " + e.getMessage());
        } finally {
            try {
                connect.close();
            } catch (SQLException ex) {
                Logger.getLogger(DoctorDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public Doctor getDoctorByID(String doctorID) {
        Doctor doctor = new Doctor();
        String sql = "select * from qlypkdongy.doctor where id_doctor=?";
        try {
            connect = DBContext.getConnection();
            PreparedStatement pstm = connect.prepareStatement(sql);
            pstm.setString(1, doctorID);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                doctor.setIdDoctor(rs.getInt("id_doctor"));
                doctor.setNameDoctor(rs.getString("name_doctor"));
                doctor.setAddress(rs.getString("address"));
                doctor.setDescriptionDoctor(rs.getString("description_doctor"));
                doctor.setTitleDoctor(rs.getString("title"));
                doctor.setAcademicRank(rs.getString("academic_rank"));
                doctor.setDegree(rs.getString("degree"));
                doctor.setPhone(rs.getString("phone"));
                doctor.setAvatar_path(rs.getString("avatar_path"));
                doctor.setUsername(rs.getString("username"));
                doctor.setPassword(rs.getString("password"));
                doctor.setStatus(rs.getBoolean("status"));
                SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");

                doctor.setUpdateDate(date.format(rs.getDate("create_date")));
                doctor.setCreateDate(date.format(rs.getDate("update_date")));
            }
        } catch (Exception e) {
            System.out.println("getDoctorByID: " + e.getMessage());
        } finally {
            try {
                connect.close();
            } catch (SQLException ex) {
                Logger.getLogger(DoctorDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return doctor;
    }

    public void updateDoctorToDB(Doctor doctorE) {
        try {
            String strUpdate = "update doctor set name_doctor=? , address = ? , description_doctor=?" +
                    ", title=? , academic_rank=?, degree=?, phone=? , avatar_path=?, username=? , password=?," +
                    "status=?, update_date=NOW() where id_doctor=?";
            connect = DBContext.getConnection();
            pstm = connect.prepareStatement(strUpdate);
            pstm.setString(1, doctorE.getNameDoctor());
            pstm.setString(2, doctorE.getAddress());
            pstm.setString(3, doctorE.getDescriptionDoctor());
            pstm.setString(4, doctorE.getTitleDoctor());
            pstm.setString(5, doctorE.getAcademicRank());
            pstm.setString(6, doctorE.getDegree());
            pstm.setString(7, doctorE.getPhone());
            pstm.setString(8, doctorE.getAvatar_path());
            pstm.setString(9, doctorE.getUsername());
            pstm.setString(10, doctorE.getPassword());
            pstm.setBoolean(11, doctorE.isStatus());
            pstm.setInt(12, doctorE.getIdDoctor());

            pstm.execute();
        } catch (Exception e) {
            System.out.println("updateDoctorToDB: " + e.getMessage());
        } finally {
            try {
                connect.close();
            } catch (SQLException e) {
                Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    public void deleteDoctor(String doctorID) {
        try {
            String strDelete = "delete from doctor where id_doctor=?";
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(strDelete);
            pstm.setString(1, doctorID);
            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println("deleteDoctor: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemImportDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    public boolean searchAvatarPath(String avatarPath) {
        ArrayList<Doctor> doc = new ArrayList<>();
        try {
            String strSelect = "SELECT * FROM doctor\n" +
                    "WHERE  avatar_path = ?";
            connect = DBContext.getConnection();
            pstm = connect.prepareStatement(strSelect);
            pstm.setString(1, avatarPath);
            rs = pstm.executeQuery();
            while (rs.next()) {
                doc.add(new Doctor(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10),
                        rs.getString(11), rs.getString(12),
                        rs.getBoolean(13),
                        rs.getString(14), rs.getString(15)
                ));
                if (doc.isEmpty())
                    return false;
            }
        } catch (Exception e) {
            System.out.println("getAllCourse: " + e.getMessage());
        } finally {
            try {
                connect.close();
            } catch (SQLException e) {
                Logger.getLogger(DoctorDAO.class.getName()).log(Level.SEVERE, null, e);
            }
            return true;
        }
    }
}
