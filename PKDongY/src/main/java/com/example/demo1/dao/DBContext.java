package com.example.demo1.dao;

import java.sql.Connection;
import java.sql.DriverManager;


public class DBContext {
    private static String DB_URL = "jdbc:mysql://localhost:3306/qLyPKDongY";
    private static String DB_URL_2 = "jdbc:mysql://localhost:4306/qLyPKDongY";
    private static String USER_NAME = "root";
    private static String PASSWORD = "12345";
    public static Connection conn = null;

    public static Connection getConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
            System.out.println("connect successfully!");
        } catch (Exception ex) {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                conn = DriverManager.getConnection(DB_URL_2, USER_NAME, PASSWORD);
                System.out.println("connect successfully!");
            } catch (Exception ex2){
                System.out.println("connect failed!");
                ex.printStackTrace();
            }
        }
        return conn;
    }

    public static void main(String[] args) {
        DBContext.getConnection();
    }
}
