package com.example.demo1.dao;


import com.example.demo1.model.Provider;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProviderDAO extends DBContext {

    private static final String GET_ALL_PROVIDER = "SELECT * FROM provider";
    private static final String GET_PROVIDER_BY_ID = "SELECT * FROM provider where id_provider =?";
    private static final String GET_LIST_NAME_PROVIDER = "SELECT name_provider FROM provider GROUP BY name_provider";
    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
        ProviderDAO providerDAO = new ProviderDAO();
        ArrayList<Provider> listPro = providerDAO.getAll();
        System.out.println(listPro.get(0).getName());
    }

    public ArrayList<Provider> getAll() {
        ArrayList<Provider> listPro = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_PROVIDER);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listPro.add(new Provider(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getString(9)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllProvider: " + e.getMessage());
        } finally {
            try { conn.close(); } catch (SQLException e) {
                Logger.getLogger(ProviderDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return listPro;
    }

    public Provider getProviderById(int id) {
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_PROVIDER_BY_ID);
            pstm.setInt(1, id);
            rs = pstm.executeQuery();
            while (rs.next()) {
                return new Provider(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getString(9)
                );
            }
        } catch (Exception e) {
            System.out.println("getProviderById: " + e.getMessage());
        } finally {
            try { conn.close(); } catch (SQLException e) {
                Logger.getLogger(ProviderDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return null;
    }

    public ArrayList<String> getListNameProvider(){
        ArrayList<String> listNameProvider = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_LIST_NAME_PROVIDER);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listNameProvider.add(rs.getString(1));
            }
            return listNameProvider;
        } catch (Exception e) {
            System.out.println("listNameProvider: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ProviderDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return null;
    }
}
