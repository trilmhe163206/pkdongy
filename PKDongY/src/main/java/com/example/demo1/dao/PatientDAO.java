package com.example.demo1.dao;

import com.example.demo1.model.Patient;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PatientDAO extends DBContext {

    private static final String GET_ALL_PATIENT = "SELECT * FROM health_index";

    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
    }

    public ArrayList<Patient> getAll() {
        ArrayList<Patient> listPa = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_PATIENT);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listPa.add(new Patient(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getInt(10),
                        rs.getString(11),
                        rs.getString(12)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllPatient: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(PatientDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return listPa;
    }
}
