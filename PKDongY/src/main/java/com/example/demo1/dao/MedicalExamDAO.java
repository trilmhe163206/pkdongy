package com.example.demo1.dao;


import com.example.demo1.model.MedicalExam;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MedicalExamDAO extends DBContext {

    private static final String GET_ALL_MEDICAL_EXAM = "SELECT * FROM medical_examination";

    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
    }

    public ArrayList<MedicalExam> getAll() {
        ArrayList<MedicalExam> listME = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_MEDICAL_EXAM);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listME.add(new MedicalExam(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllMedicalExam: " + e.getMessage());
        } finally {
            try { conn.close(); } catch (SQLException e) {
                Logger.getLogger(MedicalExamDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return listME;
    }
}
