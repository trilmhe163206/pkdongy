package com.example.demo1.dao;

import com.example.demo1.model.Diagnosis;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DiagnosisDAO extends DBContext {

    private static final String GET_ALL_DIAGNOSIS = "SELECT * FROM diagnosis";

    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
    }

    public ArrayList<Diagnosis> getAll() {
        ArrayList<Diagnosis> listD = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_DIAGNOSIS);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listD.add(new Diagnosis(
                        rs.getInt(1), rs.getInt(2),
                        rs.getInt(3), rs.getString(4),
                        rs.getString(5)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllDiagnosis: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(DiagnosisDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return listD;
    }
}
