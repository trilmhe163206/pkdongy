package com.example.demo1.dao;

import com.example.demo1.model.Account;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountDAO {
    Connection connect = null;
    Statement stm;
    PreparedStatement pstm;
    ResultSet rs;

    public ArrayList<Account> getAllUserDoctor() {
        ArrayList<Account> listA = new ArrayList<>();
        String strSelect = "select * from doctor";
        try {
            connect = DBContext.getConnection();
            pstm = connect.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listA.add(new Account(rs.getString(11), rs.getString(12))
                );
            }
        } catch (Exception e) {
            System.out.println("getAllCourse: " + e.getMessage());
        } finally {
            try {
                connect.close();
            } catch (SQLException e) {
                Logger.getLogger(DoctorDAO.class.getName()).log(Level.SEVERE, null, e);
            }
            return listA;
        }
    }

    public ArrayList<Account> getAllUserAdmin() {
        ArrayList<Account> listA = new ArrayList<>();
        String strSelect = "select * from admin";
        try {
            connect = DBContext.getConnection();
            pstm = connect.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listA.add(new Account(rs.getString(2), rs.getString(3))
                );
            }
        } catch (Exception e) {
            System.out.println("getAllCourse: " + e.getMessage());
        } finally {
            try {
                connect.close();
            } catch (SQLException e) {
                Logger.getLogger(DoctorDAO.class.getName()).log(Level.SEVERE, null, e);
            }
            return listA;
        }
    }

    public Account getAccountByEmailAndPwdDoctor(Account account) {
        try {
            String sql = "SELECT * FROM doctor where username = ? and password = ?";
            pstm = DBContext.getConnection().prepareStatement(sql);
            pstm.setString(1, account.getUsername());
            pstm.setString(2, account.getPassword());
            rs = pstm.executeQuery();
            if (rs.next()) {
                return new Account(rs.getString(11), rs.getString(12));
            }
        } catch (Exception e) {
            System.out.println("getAccountByEmailAndPwdD:" + e.getMessage());
        }
        return null;
    }

    public Account getAccountByEmailAndPwdAdmin(Account account) {
        try {
            String sql = "SELECT * FROM admin where username = ? and password = ?";
            pstm = DBContext.getConnection().prepareStatement(sql);
            pstm.setString(1, account.getUsername());
            pstm.setString(2, account.getPassword());
            rs = pstm.executeQuery();
            if (rs.next()) {
                return new Account(rs.getString(2), rs.getString(3));
            }
        } catch (Exception e) {
            System.out.println("getAccountByEmailAndPwd:" + e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        AccountDAO adao = new AccountDAO();
//        ArrayList<Account> al = adao.getAllUserDoctor();
        Account al = adao.getAccountByEmailAndPwdAdmin(new Account("admin", "admin123"));
        System.out.println(al);
    }
}
