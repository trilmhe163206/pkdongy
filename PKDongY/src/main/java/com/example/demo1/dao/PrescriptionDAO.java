package com.example.demo1.dao;


import com.example.demo1.model.Prescription;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrescriptionDAO extends DBContext {

    private static final String GET_ALL_PRESCRIPTION = "SELECT * FROM prescription";

    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
    }

    public ArrayList<Prescription> getAll() {
        ArrayList<Prescription> listPres = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_PRESCRIPTION);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listPres.add(new Prescription(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        rs.getFloat(5),
                        rs.getString(6)

                ));
            }
        } catch (Exception e) {
            System.out.println("getAllPrescription: " + e.getMessage());
        } finally {
            try { conn.close(); } catch (SQLException e) {
                Logger.getLogger(PrescriptionDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return listPres;
    }
}
