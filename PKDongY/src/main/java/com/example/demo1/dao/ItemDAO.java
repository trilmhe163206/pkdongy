package com.example.demo1.dao;


import com.example.demo1.model.Item;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ItemDAO extends DBContext {

    private static final String GET_ALL_ITEM = "SELECT * FROM item";

    private static final String GET_ITEM_BY_ID = "SELECT * FROM item where id_item = ?";
    private static final String GET_LIST_NAME_ITEM = "SELECT name_item FROM item GROUP BY name_item";
    private static final String GET_TYPE_BY_NAME = "SELECT TYPE FROM item WHERE name_item LIKE ?";

    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
    }

    public ArrayList<Item> getAll() {
        ArrayList<Item> listI = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_ITEM);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listI.add(new Item(rs.getInt(1), rs.getString(2),
                        rs.getString(3), rs.getString(4),
                        rs.getFloat(5), rs.getString(6),
                        rs.getInt(7), rs.getString(8),
                        rs.getString(9)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllItem: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return listI;
    }

    public Item getItemById(int id) {
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ITEM_BY_ID);
            pstm.setInt(1, id);
            rs = pstm.executeQuery();
            if (rs.next()) {
                return new Item(rs.getInt(1), rs.getString(2),
                        rs.getString(3), rs.getString(4),
                        rs.getFloat(5), rs.getString(6),
                        rs.getInt(7), rs.getString(8),
                        rs.getString(9)
                );
            }
        } catch (Exception e) {
            System.out.println("getItemById: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return null;
    }

    public ArrayList<String> getListNameItem(){
        ArrayList<String> listNameItem = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_LIST_NAME_ITEM);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listNameItem.add(rs.getString(1));
            }
            return listNameItem;
        } catch (Exception e) {
            System.out.println("getListNameItem: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return null;
    }

    public String getTypeByNameItem(String nameItem){
        String type = null;
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_TYPE_BY_NAME);
            pstm.setString(1, nameItem);
            rs = pstm.executeQuery();
            if (rs.next()) {
                type = rs.getString(1);
            }
            return type;
        } catch (Exception e) {
            System.out.println("getTypeByNameItem: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return type;
    }

}
