package com.example.demo1.dao;

import com.example.demo1.model.HealthIndex;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HealthIndexDAO extends DBContext {

    private static final String GET_ALL_HEALTH_INDEX = "SELECT * FROM health_index";

    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
    }

    public ArrayList<HealthIndex> getAll() {
        ArrayList<HealthIndex> listII = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_HEALTH_INDEX);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listII.add(new HealthIndex(
                        rs.getInt(1), rs.getInt(2),
                        rs.getInt(3), rs.getInt(4),
                        rs.getInt(5), rs.getInt(6),
                        rs.getString(7), rs.getString(8)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllHealthIndex: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(HealthIndexDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return listII;
    }
}
