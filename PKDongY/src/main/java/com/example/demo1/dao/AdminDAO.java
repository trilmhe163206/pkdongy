package com.example.demo1.dao;

import com.example.demo1.model.Admin;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdminDAO extends DBContext {

    private static final String GET_ALL_ADMIN = "SELECT * FROM health_index";

    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
    }

    public ArrayList<Admin> getAll() {
        ArrayList<Admin> listAd = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_ADMIN);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listAd.add(new Admin(
                        rs.getInt(1), rs.getString(2),
                        rs.getString(3)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllAdmin: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return listAd;
    }
}
