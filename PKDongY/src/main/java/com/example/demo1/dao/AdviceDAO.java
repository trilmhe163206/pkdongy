package com.example.demo1.dao;

import com.example.demo1.model.Advice;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdviceDAO extends DBContext {

    private static final String GET_ALL_ADVICE = "SELECT * FROM advice";

    PreparedStatement pstm;
    ResultSet rs;

    public static void main(String[] args) {
    }

    public ArrayList<Advice> getAll() {
        ArrayList<Advice> listAdv = new ArrayList<>();
        try {
            conn = DBContext.getConnection();
            pstm = conn.prepareStatement(GET_ALL_ADVICE);
            rs = pstm.executeQuery();
            while (rs.next()) {
                listAdv.add(new Advice(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3)
                ));
            }
        } catch (Exception e) {
            System.out.println("getAllAdvice: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                Logger.getLogger(AdviceDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return listAdv;
    }
}
