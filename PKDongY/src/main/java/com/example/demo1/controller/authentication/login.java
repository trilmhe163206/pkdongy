package com.example.demo1.controller.authentication;

import com.example.demo1.dao.AccountDAO;
import com.example.demo1.dao.DoctorDAO;
import com.example.demo1.model.Account;
import com.example.demo1.model.Doctor;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.WebServlet;

import java.io.IOException;

@WebServlet(name = "login", value = "/login")
public class login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String go = request.getParameter("go");
        if (go != null){
            if ( go.equals("logout")){
                request.getSession().setAttribute("isLoggedIn", "false");
                request.getSession().setAttribute("account", null);
                request.getRequestDispatcher("/home").forward(request, response);
            }
        }
        if (request.getSession().getAttribute("isLoggedIn") != "true"){
            request.getRequestDispatcher("/views/authentication/login.jsp").forward(request, response);
        } else if (request.getSession().getAttribute("isLoggedIn") == "true"){
            request.getRequestDispatcher("/home").forward(request, response);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Account account = new Account(username,password);
        AccountDAO aDao = new AccountDAO();
        if ( aDao.getAccountByEmailAndPwdAdmin(account) != null ){
            request.getSession().setAttribute("isLoggedIn", "true");
            request.getSession().setAttribute("role", "admin");
            request.getSession().setAttribute("account", account);
            String contextPath = request.getContextPath();
//            request.getRequestDispatcher("/home").forward(request, response);
            response.sendRedirect(contextPath + "/home");
//            response.sendRedirect(contextPath + "/doctor-list");
        } else if ( aDao.getAccountByEmailAndPwdDoctor(account) != null ){
            request.getSession().setAttribute("isLoggedIn", "true");
            request.getSession().setAttribute("role", "doctor");
            request.getSession().setAttribute("account", account);
            String contextPath = request.getContextPath();
//            request.getRequestDispatcher("/home").forward(request, response);
            response.sendRedirect(contextPath + "/home");
//            response.sendRedirect(contextPath + "/doctor-list");
        } else {
            request.getSession().setAttribute("isLoggedIn", "false");
            String contextPath = request.getContextPath();
            request.setAttribute("username", username);
            request.setAttribute("password", password);
            request.setAttribute("msgError", "Sai tài khoản hoặc mật khẩu");
            request.getRequestDispatcher("/views/authentication/login.jsp").forward(request, response);
        }
    }
}