package com.example.demo1.controller.doctor;

import com.example.demo1.dao.DoctorDAO;
import com.example.demo1.model.Doctor;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

@MultipartConfig(
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 100 // 100 MB
)
@WebServlet(name = "DoctorAddServlet", value = "/doctor-add")
public class DoctorAddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/views/doctor/doctor-add.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("importName");
        String address = request.getParameter("importAddress");
        String title = request.getParameter("importTitle");
        String academic_rank = request.getParameter("importAcaRank");
        String degree = request.getParameter("importDegree");
        String phone = request.getParameter("importPhone");
        String username = request.getParameter("importUsername");
        String password = request.getParameter("importPassword");
        String description = request.getParameter("importDescrip");
        String path_avatar = null;
        Part filePart = null;
        DoctorDAO doc = new DoctorDAO();


        filePart = request.getPart("filename");
        if (filePart != null && filePart.getSize() > 0) {
            String fileName = filePart.getSubmittedFileName();

            // Save the file to disk
            // Get the ServletContext object
            ServletContext context = request.getServletContext();

            // Get the real path of the project
            String realPath = context.getRealPath("/");
            OutputStream out = new FileOutputStream(new File(realPath + "\\img\\" + fileName));
            InputStream fileContent = filePart.getInputStream();
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = fileContent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            String srcS = realPath + "\\img\\" + fileName;
            String desS = realPath.replace("\\target\\PKDongY-1.0-SNAPSHOT\\", "") + "\\src\\main\\webapp\\img\\" + fileName;
            copyImage(srcS, desS);
            out.flush();
            out.close();
            /* Set path avatar by direct was upload */
            path_avatar = "/img/" + filePart.getSubmittedFileName();
        } else {
            path_avatar = "/img/Avatar.png";
        }
        Doctor doctor = new Doctor(name, address, description, title, academic_rank, degree, phone, path_avatar, username, password);
        doc.insertDoctorToDB(doctor);
        request.setAttribute("currentPage1", "doctor");
        response.sendRedirect("doctor-list");

    }

    public static void copyImage(String srcS, String destS) {
        System.out.println("...");
        Path sourcePath = Paths.get(srcS);
        Path destPath = Paths.get(destS);

        // Copy the image file
        try {
            Files.copy(sourcePath, destPath);
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("Image copied successfully!");
    }
}