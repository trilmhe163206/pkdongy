package com.example.demo1.controller.doctor;

import com.example.demo1.dao.DoctorDAO;
import com.example.demo1.model.Doctor;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;

@WebServlet(name = "DoctorDetailServlet", value = "/doctor-detail")
public class DoctorDetailServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String doctorID = request.getParameter("doctorID");
        String delete = request.getParameter("delete");
        System.out.println(delete);
        DoctorDAO doc = new DoctorDAO();
        Doctor doctorA = new Doctor();
        doctorA = doc.getDoctorByID(doctorID);
        boolean checkPath = doc.searchAvatarPath(doctorA.getAvatar_path());
        if (delete != null) {
            if (checkPath) {
                doc.deleteDoctor(doctorID);
                System.out.println(delete);
                response.sendRedirect("doctor-list");
                return;
            } else {
                ServletContext context = request.getServletContext();
                String realPath = context.getRealPath("/");
                String convertedPath = doctorA.getAvatar_path().replace("/", "\\").substring(1);
                String pathBuild = realPath + convertedPath;
                String targetSubstring = "\\target\\PKDongY-1.0-SNAPSHOT\\img";
                String replacementSubstring = "\\src\\main\\webapp\\img";
                String pathReal = pathBuild.replace(targetSubstring, replacementSubstring);
                deleteFile(pathBuild);
                deleteFile(pathReal);
                doc.deleteDoctor(doctorID);
                System.out.println(delete);
                response.sendRedirect("doctor-list");
                return;
            }

        }
        request.setAttribute("currentPage1", "doctor");
        request.setAttribute("doctorID", doctorID);
        request.setAttribute("doctor", doctorA);
        request.getRequestDispatcher("/views/doctor/doctor-detail.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public static void deleteFile(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            if (file.delete()) {
                System.out.println("File đã được xóa thành công.");
            } else {
                System.out.println("Không thể xóa file.");
            }
        } else {
            System.out.println("File không tồn tại.");
        }
    }
}