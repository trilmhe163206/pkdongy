package com.example.demo1.controller;

import com.example.demo1.dao.ClinicDAO;
import com.example.demo1.dao.DoctorDAO;
import com.example.demo1.model.Clinic;
import com.example.demo1.model.Doctor;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "HomePage", value = "/home")
public class home extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ClinicDAO clDAO = new ClinicDAO();
        Clinic clinic = clDAO.getClinicById(1);
        request.getSession().setAttribute("clinic", clinic);
        request.setAttribute("currentPage1", "home");
        request.getRequestDispatcher("/views/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("currentPage1", "home");
        request.getRequestDispatcher("/views/home.jsp").forward(request, response);
    }
}