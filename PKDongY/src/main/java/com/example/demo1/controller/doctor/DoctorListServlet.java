package com.example.demo1.controller.doctor;

import com.example.demo1.dao.DoctorDAO;
import com.example.demo1.model.Doctor;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "DoctorListServlet", value = "/doctor-list")
public class DoctorListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("search-information");
        String page_raw = request.getParameter("page");
        String status = request.getParameter("status");
        if (status == null || status.equals("")) {
            status = null;
        }
        int page = 1;
        if (page_raw != null) {
            page = Integer.parseInt(page_raw);
        }
        DoctorDAO dao = new DoctorDAO();
        ArrayList<Doctor> listD = null;
        if ((search == null || search.equals("")) && (status == null || status.equals(""))) {
            listD = dao.getAllDoctor();
        } else if (search != null && !search.equals("") && (status == null || status.equals(""))) {
            listD = dao.searchDoctor(search);
        } else if (search != null && !search.equals("") && status != null && !status.equals("")) {
            listD = dao.searchDoctorAndStatus(search, status);
        } else if ((search == null || search.equals("")) && status != null && !status.equals("")) {
            listD = dao.searchStatus(status);
        }
        int totalPage = listD.size() / 6;
        if (listD.size() % 6 != 0) {
            totalPage++;
        }
        ArrayList<Doctor> listDoctor = null;
        if (listD.size() / 6 != 0 && page == totalPage && !(listD.size() <= 6)) {
            listDoctor = new ArrayList<>(listD.subList((page - 1) * 6, listD.size()));
        } else {
            if (!(listD.size() <= 6)) {
                listDoctor = new ArrayList<>(listD.subList((page - 1) * 6, page * 6));
            }
        }
        if (listD.size() <= 6) {
            listDoctor = listD;
        }
        request.setAttribute("search", search);
        request.setAttribute("status", status);
        request.setAttribute("listD", listDoctor);
        request.setAttribute("currentPage", page);
        request.setAttribute("totalPage", totalPage);
        request.setAttribute("currentPage1", "doctor");
        request.getRequestDispatcher("/views/doctor/doctor-list.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Code xóa doctor
    }
}