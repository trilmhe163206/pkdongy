package com.example.demo1.controller.itemImport;

import com.example.demo1.dao.ItemDAO;
import com.example.demo1.dao.ItemImportDAO;
import com.example.demo1.dao.ProviderDAO;
import com.example.demo1.model.Item;
import com.example.demo1.model.ItemImport;
import com.example.demo1.model.Provider;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet(name = "ItemImportServlet", value = "/item-import")
public class ItemImportServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ItemImportDAO itemImportDAO = new ItemImportDAO();
        ProviderDAO providerDAO = new ProviderDAO();
        ItemDAO itemDAO = new ItemDAO();
        ArrayList<Item> listIt = itemDAO.getAll();
        ArrayList<Provider> listP = providerDAO.getAll();
        ArrayList<ItemImport> listI = itemImportDAO.getAll();
        String searchKey = request.getParameter("searchKey");
        if (searchKey != null) {
            listI = itemImportDAO.searchImport(searchKey);
        }
        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd ");
        String formattedDate = sdf.format(currentDate);

        if (!listI.isEmpty()) {
            int ntd = 8;
            int numP = (listI.size() % ntd == 0) ? listI.size() / ntd : listI.size() / ntd + 1;

            String page_raw = request.getParameter("page");
            int page = 1;
            if (page_raw != null) {
                page = Integer.parseInt(page_raw);
            }
            if (page > numP) {
                page = numP;
            }
            int startIndex = (page - 1) * ntd;
            int endIndex = (page * ntd);
            if (endIndex > listI.size()) {
                endIndex = listI.size();
            }
            ArrayList<ItemImport> itemImportsDisplay = new ArrayList<>(listI.subList(startIndex, endIndex));
            request.setAttribute("searchKey", searchKey);
            request.setAttribute("numP", numP);
            request.setAttribute("page", page);
            request.setAttribute("currentDate", formattedDate);
            request.setAttribute("listI", itemImportsDisplay);
            request.setAttribute("listIt", listIt);
            request.setAttribute("listP", listP);
//            request.getRequestDispatcher("/views/item-import/import-list.jsp").forward(request, response);
        } else {
            request.setAttribute("nullListMsg", "Hiện chưa có dữ liệu về giao dịch, hãy tạo mới");
            request.setAttribute("searchKey", searchKey);
            request.setAttribute("listIt", listIt);
            request.setAttribute("listP", listP);
            request.setAttribute("currentDate", formattedDate);
            request.setAttribute("numP", 0);
            request.setAttribute("page", 0);
        }
        request.setAttribute("currentPage1", "itemImport");
        request.getRequestDispatcher("/views/item-import/import-list.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String go = request.getParameter("go");
        switch (go) {
            case ("update"):
                handleUpdate(request, response);
                break;
            case ("add"):
                handleAdd(request, response);
                break;
            case ("delete"):
                handleDelete(request, response);
                break;
        }
    }

    public static void handleUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        int idItem = Integer.parseInt(request.getParameter("idItem"));
        float importPrice = Float.parseFloat(request.getParameter("importPrice"));
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        float totalPrice = Float.parseFloat(request.getParameter("totalPrice"));
        int idProvider = Integer.parseInt(request.getParameter("idProvider"));
        String importDate = request.getParameter("importDate");
        int status = Integer.parseInt(request.getParameter("status"));
        ItemImport itemImport = new ItemImport(id, idProvider, idItem, importPrice, quantity, totalPrice, status);
        ItemImportDAO itemImportDAO = new ItemImportDAO();
        if (itemImportDAO.updateImport(itemImport) == 1) {
            System.out.println("update xong");
        } else {
            System.out.println("update that bai");
        }
        response.sendRedirect("item-import");
    }

    public static void handleAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int idItem = Integer.parseInt(request.getParameter("idItem"));
        float importPrice = Float.parseFloat(request.getParameter("importPrice"));
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        float totalPrice = Float.parseFloat(request.getParameter("totalPrice"));
        int idProvider = Integer.parseInt(request.getParameter("idProvider"));
        int status = Integer.parseInt(request.getParameter("status"));
        ItemImport itemImport = new ItemImport(0, idProvider, idItem, importPrice, quantity, totalPrice, status);
        ItemImportDAO itemImportDAO = new ItemImportDAO();
        if (itemImportDAO.addImport(itemImport) == 1) {
            System.out.println("add xong");
        } else {
            System.out.println("add that bai");
        }
        response.sendRedirect("item-import");
    }

    public static void handleDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("itemImportId"));
        ItemImportDAO itemImportDAO = new ItemImportDAO();
        if (itemImportDAO.deleteImport(id) == 1) {
            System.out.println("delete xong");
        } else {
            System.out.println("delete that bai");
        }
        response.sendRedirect("item-import");
    }
}