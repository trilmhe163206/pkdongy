package com.example.demo1.controller.itemImport;

import com.example.demo1.dao.ItemDAO;
import com.example.demo1.dao.ItemImportDAO;
import com.example.demo1.dao.ProviderDAO;
import com.example.demo1.model.Item;
import com.example.demo1.model.ItemImport;
import com.example.demo1.model.Provider;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ibm.icu.text.Transliterator;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(name = "ItemImportAjax", value = "/item-import-ajax")
public class ItemImportAjax extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String go = request.getParameter("go");
        switch (go) {
            case ("changeType"):
                handleChangeType(request, response);
                break;
            case ("search"):
                handleSearch(request, response);
                break;

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public static String removeAccents(String input) {
        Transliterator transliterator = Transliterator.getInstance("NFD; [:Nonspacing Mark:] Remove; NFC;");
        return transliterator.transform(input);
    }

    protected void handleChangeType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int itemId = Integer.parseInt(request.getParameter("itemId"));
        ItemDAO itemDAO = new ItemDAO();
        Item item = itemDAO.getItemById(itemId);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("type", item.getType());

        response.setContentType("application/json");
        response.getWriter().write(jsonObject.toString());
    }

    protected void handleSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchKey = request.getParameter("searchKey");
        if (searchKey != null && !searchKey.isEmpty()){
            searchKey = removeAccents(searchKey);
            ItemDAO itemDAO = new ItemDAO();
            ProviderDAO providerDAO = new ProviderDAO();
            ArrayList<String> listNameItems = itemDAO.getListNameItem();
            ArrayList<String> listNameProviders = providerDAO.getListNameProvider();

            try (PrintWriter out = response.getWriter()) {
                for (String itemName : listNameItems) {
                    String removeAccentsItemName = removeAccents(itemName).toLowerCase();
                    if (removeAccentsItemName.contains(searchKey)){
                        out.println("<div class=\"search-result\" onclick=\"window.location = 'item-import?searchKey="+ itemName + "'\">"+ itemName + "<span class=\"search-result-note\">" + itemDAO.getTypeByNameItem(itemName) + "</span></div>\n");
                    }
                }

                for (String providerName : listNameProviders) {
                    String removeAccentsProviderName = removeAccents(providerName).toLowerCase();
                    if (removeAccentsProviderName.contains(searchKey)){
                        out.println("<div class=\"search-result\" onclick=\"window.location = 'item-import?searchKey="+ providerName + "'\">"+ providerName + "<span class=\"search-result-note\">Nhà cung cấp</span></div>\n");
                    }
                }
            }
        }
    }
}