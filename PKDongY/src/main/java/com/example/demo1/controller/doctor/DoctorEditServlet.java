package com.example.demo1.controller.doctor;

import com.example.demo1.dao.DoctorDAO;
import com.example.demo1.model.Doctor;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@MultipartConfig(
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 100 // 100 MB
)
@WebServlet(name = "DoctorEditServlet", value = "/doctor-edit")
public class DoctorEditServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String doctorID = request.getParameter("doctorID");
        Doctor doctor = new Doctor();
        DoctorDAO doctorDAO = new DoctorDAO();
        doctor = doctorDAO.getDoctorByID(doctorID);

        request.setAttribute("doctorID", doctorID);
        request.setAttribute("doctor", doctor);

        request.getRequestDispatcher("/views/doctor/doctor-edit.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String doctorID = request.getParameter("doctorID");
        String name = request.getParameter("importName");
        String address = request.getParameter("importAddress");
        String title = request.getParameter("importTitle");
        String academic_rank = request.getParameter("importAcaRank");
        String degree = request.getParameter("importDegree");
        String phone = request.getParameter("importPhone");
        String username = request.getParameter("importUsername");
        String password = request.getParameter("importPassword");
        String status = request.getParameter("status");
        String description = request.getParameter("importDescrip");
        String path_avatar = null;
        Part filePart = null;
        DoctorDAO doc = new DoctorDAO();
        Doctor doctorA = new Doctor();
        doctorA = doc.getDoctorByID(doctorID);
        boolean checkPath = doc.searchAvatarPath(doctorA.getAvatar_path());

        filePart = request.getPart("filename");
        if (filePart != null && filePart.getSize() > 0 && doctorA.getAvatar_path().equals("/img/Avatar.png")) {
//            System.out.println("vao 1: thay ảnh mặc định");
            String fileName = filePart.getSubmittedFileName();

            // Save the file to disk
            // Get the ServletContext object
            ServletContext context = request.getServletContext();

            // Get the real path of the project
            String realPath = context.getRealPath("/");
            OutputStream out = new FileOutputStream(new File(realPath + "\\img\\" + fileName));
            InputStream fileContent = filePart.getInputStream();
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = fileContent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            String srcS = realPath + "\\img\\" + fileName;
            String desS = realPath.replace("\\target\\PKDongY-1.0-SNAPSHOT\\", "") + "\\src\\main\\webapp\\img\\" + fileName;
            copyImage(srcS, desS);
            out.flush();
            out.close();
            /* Set path avatar by direct was upload */
            path_avatar = "/img/" + filePart.getSubmittedFileName();
        } else if (filePart != null && filePart.getSize() > 0 && !doctorA.getAvatar_path().equals("/img/" + filePart.getSubmittedFileName())) {
//            System.out.println("vao 2: xóa ảnh cũ và thêm ảnh mới");
            if (checkPath) {
                String fileName = filePart.getSubmittedFileName();

                // Save the file to disk
                // Get the ServletContext object
                ServletContext context = request.getServletContext();

                // Get the real path of the project
                String realPath = context.getRealPath("/");
                OutputStream out = new FileOutputStream(new File(realPath + "\\img\\" + fileName));
                InputStream fileContent = filePart.getInputStream();
                int read = 0;
                byte[] bytes = new byte[1024];
                while ((read = fileContent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                String srcS = realPath + "\\img\\" + fileName;
                String desS = realPath.replace("\\target\\PKDongY-1.0-SNAPSHOT\\", "") + "\\src\\main\\webapp\\img\\" + fileName;
                copyImage(srcS, desS);
                out.flush();
                out.close();
                /* Set path avatar by direct was upload */
                path_avatar = "/img/" + filePart.getSubmittedFileName();
            } else {
                String fileName = filePart.getSubmittedFileName();

                // Save the file to disk
                // Get the ServletContext object
                ServletContext context = request.getServletContext();

                // Get the real path of the project
                String realPath = context.getRealPath("/");
                OutputStream out = new FileOutputStream(new File(realPath + "\\img\\" + fileName));
                InputStream fileContent = filePart.getInputStream();
                int read = 0;
                byte[] bytes = new byte[1024];
                while ((read = fileContent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                String srcS = realPath + "\\img\\" + fileName;
                String desS = realPath.replace("\\target\\PKDongY-1.0-SNAPSHOT\\", "") + "\\src\\main\\webapp\\img\\" + fileName;
                copyImage(srcS, desS);

                String convertedPath = doctorA.getAvatar_path().replace("/", "\\").substring(1);
                String pathBuild = realPath + convertedPath;

                String targetSubstring = "\\target\\PKDongY-1.0-SNAPSHOT\\img";
                String replacementSubstring = "\\src\\main\\webapp\\img";

                String pathReal = pathBuild.replace(targetSubstring, replacementSubstring);
                System.out.println(pathReal);

                //xóa ảnh trong file build và file src
                deleteFile(pathBuild);
                deleteFile(pathReal);
                out.flush();
                out.close();
                /* Set path avatar by direct was upload */
                path_avatar = "/img/" + filePart.getSubmittedFileName();
            }

        } else {
//            System.out.println("vao 3: không thay đổi mặc định");
            path_avatar = doctorA.getAvatar_path();
        }
        Doctor doctorE = new Doctor(Integer.parseInt(doctorID), name, address, description, title, academic_rank, degree, phone, path_avatar, username, password, status.equals("1"));
        doc.updateDoctorToDB(doctorE);
        request.setAttribute("currentPage1", "doctor");
        response.sendRedirect("doctor-list");

    }

    public static void copyImage(String srcS, String destS) {
        System.out.println("...");
        Path sourcePath = Paths.get(srcS);
        Path destPath = Paths.get(destS);

        // Copy the image file
        try {
            Files.copy(sourcePath, destPath);
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("Image copied successfully!");
    }

    public static void deleteFile(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            if (file.delete()) {
                System.out.println("File đã được xóa thành công.");
            } else {
                System.out.println("Không thể xóa file.");
            }
        } else {
            System.out.println("File không tồn tại.");
        }
    }
}