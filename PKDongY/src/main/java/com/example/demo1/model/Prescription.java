package com.example.demo1.model;

public class Prescription {
    int id;
    int idMe;
    int idItem;
    int quantity;
    float totalPrice;
    String createDate;

    public Prescription() {
    }

    public Prescription(int id, int idMe, int idItem, int quantity, float totalPrice, String createDate) {
        this.id = id;
        this.idMe = idMe;
        this.idItem = idItem;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.createDate = createDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdMe() {
        return idMe;
    }

    public void setIdMe(int idMe) {
        this.idMe = idMe;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
