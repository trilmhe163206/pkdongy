package com.example.demo1.model;

public class Diagnosis {
    int id;
    int idMe;
    int disease;
    String medical_information;
    String diagnosisTime;

    public Diagnosis() {
    }

    public Diagnosis(int id, int idMe, int disease, String medical_information, String diagnosisTime) {
        this.id = id;
        this.idMe = idMe;
        this.disease = disease;
        this.medical_information = medical_information;
        this.diagnosisTime = diagnosisTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdMe() {
        return idMe;
    }

    public void setIdMe(int idMe) {
        this.idMe = idMe;
    }

    public int getDisease() {
        return disease;
    }

    public void setDisease(int disease) {
        this.disease = disease;
    }

    public String getMedical_information() {
        return medical_information;
    }

    public void setMedical_information(String medical_information) {
        this.medical_information = medical_information;
    }

    public String getDiagnosisTime() {
        return diagnosisTime;
    }

    public void setDiagnosisTime(String diagnosisTime) {
        this.diagnosisTime = diagnosisTime;
    }
}
