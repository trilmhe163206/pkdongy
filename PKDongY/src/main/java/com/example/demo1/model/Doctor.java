package com.example.demo1.model;

public class Doctor {
    private int idDoctor;
    private String nameDoctor;
    private String gender;
    private String address;
    private String descriptionDoctor;
    private String titleDoctor;
    private String academicRank;
    private String degree;
    private String phone;
    private String avatar_path;
    private String username;
    private String password;
    private boolean status;
    private String createDate;
    private String updateDate;

    public Doctor() {
    }

    public Doctor(String nameDoctor, String address, String descriptionDoctor, String titleDoctor, String academicRank, String degree, String phone, String avatar_path, String username, String password) {
        this.nameDoctor = nameDoctor;
        this.address = address;
        this.descriptionDoctor = descriptionDoctor;
        this.titleDoctor = titleDoctor;
        this.academicRank = academicRank;
        this.degree = degree;
        this.phone = phone;
        this.avatar_path = avatar_path;
        this.username = username;
        this.password = password;
    }

    public Doctor(int idDoctor, String nameDoctor, String address, String descriptionDoctor, String titleDoctor, String academicRank, String degree, String phone, String avatar_path, String username, String password, boolean status) {
        this.idDoctor = idDoctor;
        this.nameDoctor = nameDoctor;
        this.address = address;
        this.descriptionDoctor = descriptionDoctor;
        this.titleDoctor = titleDoctor;
        this.academicRank = academicRank;
        this.degree = degree;
        this.phone = phone;
        this.avatar_path = avatar_path;
        this.username = username;
        this.password = password;
        this.status = status;
    }

    public Doctor(int idDoctor, String nameDoctor, String gender, String address, String descriptDoctor, String titleDoctor, String academicRank, String degree, String phone, String avatar_path, String username, String password, boolean status, String createDate, String updateDate) {
        this.idDoctor = idDoctor;
        this.nameDoctor = nameDoctor;
        this.gender = gender;
        this.address = address;
        this.descriptionDoctor = descriptDoctor;
        this.titleDoctor = titleDoctor;
        this.academicRank = academicRank;
        this.degree = degree;
        this.phone = phone;
        this.avatar_path = avatar_path;
        this.username = username;
        this.password = password;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public int getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(int idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getNameDoctor() {
        return nameDoctor;
    }

    public void setNameDoctor(String nameDoctor) {
        this.nameDoctor = nameDoctor;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDescriptionDoctor() {
        return descriptionDoctor;
    }

    public void setDescriptionDoctor(String descriptionDoctor) {
        this.descriptionDoctor = descriptionDoctor;
    }

    public String getTitleDoctor() {
        return titleDoctor;
    }

    public void setTitleDoctor(String titleDoctor) {
        this.titleDoctor = titleDoctor;
    }

    public String getAcademicRank() {
        return academicRank;
    }

    public void setAcademicRank(String academicRank) {
        this.academicRank = academicRank;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar_path() {
        return avatar_path;
    }

    public void setAvatar_path(String avatar_path) {
        this.avatar_path = avatar_path;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "idDoctor=" + idDoctor +
                ", nameDoctor='" + nameDoctor + '\'' +
                ", gender='" + gender + '\'' +
                ", address='" + address + '\'' +
                ", descriptDoctor='" + descriptionDoctor + '\'' +
                ", titleDoctor='" + titleDoctor + '\'' +
                ", academicRank='" + academicRank + '\'' +
                ", degree='" + degree + '\'' +
                ", phone='" + phone + '\'' +
                ", avatar_path='" + avatar_path + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", createDate='" + createDate + '\'' +
                ", updateDate='" + updateDate + '\'' +
                '}';
    }
}
