package com.example.demo1.model;

public class HealthIndex {
    int id;
    int idMe;
    int height;
    int weight;
    int heartRate;
    int bloodPressure;
    String otherInfo;
    String examinationTime;

    public HealthIndex() {
    }

    public HealthIndex(int id, int idMe, int height, int weight, int heartRate, int bloodPressure, String otherInfo, String examinationTime) {
        this.id = id;
        this.idMe = idMe;
        this.height = height;
        this.weight = weight;
        this.heartRate = heartRate;
        this.bloodPressure = bloodPressure;
        this.otherInfo = otherInfo;
        this.examinationTime = examinationTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdMe() {
        return idMe;
    }

    public void setIdMe(int idMe) {
        this.idMe = idMe;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(int bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getExaminationTime() {
        return examinationTime;
    }

    public void setExaminationTime(String examinationTime) {
        this.examinationTime = examinationTime;
    }
}
