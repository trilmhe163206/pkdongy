package com.example.demo1.model;

public class ItemImport {
    int id;
    int idProvider;
    int idItem;
    float importPrice;
    int quantity;
    float totalPrice;
    int status;
    String importDate;
    String updateDate;

    public ItemImport() {
    }

    public ItemImport(int id, int idProvider, int idItem, float importPrice, int quantity, float totalPrice, int status, String importDate, String updateDate) {
        this.id = id;
        this.idProvider = idProvider;
        this.idItem = idItem;
        this.importPrice = importPrice;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.status = status;
        this.importDate = importDate;
        this.updateDate = updateDate;
    }

    public ItemImport(int id, int idProvider, int idItem, float importPrice, int quantity, float totalPrice, int status) {
        this.id = id;
        this.idProvider = idProvider;
        this.idItem = idItem;
        this.importPrice = importPrice;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.status = status;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public int getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(int idProvider) {
        this.idProvider = idProvider;
    }

    public float getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(float importPrice) {
        this.importPrice = importPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getImportDate() {
        return importDate;
    }

    public void setImportDate(String importDate) {
        this.importDate = importDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}
