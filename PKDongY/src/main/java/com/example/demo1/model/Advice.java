package com.example.demo1.model;

public class Advice {
    int id;
    int idPrescription;
    String description;

    public Advice() {
    }

    public Advice(int id, int idPrescription, String description) {
        this.id = id;
        this.idPrescription = idPrescription;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPrescription() {
        return idPrescription;
    }

    public void setIdPrescription(int idPrescription) {
        this.idPrescription = idPrescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
