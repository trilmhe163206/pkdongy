package com.example.demo1.model;

public class MedicalExam {
    int id;
    int idPatient;
    int idDoctor;
    String reasonToVisit;
    String receptionTime;
    String appointmentTime;

    public MedicalExam() {
    }

    public MedicalExam(int id, int idPatient, int idDoctor, String reasonToVisit, String receptionTime, String appointmentTime) {
        this.id = id;
        this.idPatient = idPatient;
        this.idDoctor = idDoctor;
        this.reasonToVisit = reasonToVisit;
        this.receptionTime = receptionTime;
        this.appointmentTime = appointmentTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(int idPatient) {
        this.idPatient = idPatient;
    }

    public int getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(int idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getReasonToVisit() {
        return reasonToVisit;
    }

    public void setReasonToVisit(String reasonToVisit) {
        this.reasonToVisit = reasonToVisit;
    }

    public String getReceptionTime() {
        return receptionTime;
    }

    public void setReceptionTime(String receptionTime) {
        this.receptionTime = receptionTime;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }
}
