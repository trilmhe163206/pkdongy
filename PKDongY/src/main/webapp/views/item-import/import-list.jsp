<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="com.example.demo1.dao.ItemDAO" %>
<%@ page import="com.example.demo1.model.Item" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="com.example.demo1.dao.ProviderDAO" %>
<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 10/16/2023
  Time: 3:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Quản lý nhập</title>
    <%@include file="../template/import.jsp" %>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/content.css">
</head>

<%
    request.setAttribute("itemDAO", new ItemDAO());
    request.setAttribute("providerDAO", new ProviderDAO());
%>

<body>
<div class="wrap wrap-page">
    <jsp:include page="../template/sidebar.jsp"/>
    <div class="content-page">
        <nav class="nav-bar">

            <div class="page-name">
                <h2 id="test">QUẢN LÝ GIAO DỊCH NHẬP</h2>

            </div>
            <div class="button-area" style="float: right">
                <button class="rev-btn" style="margin-right: 20px"><i class="fa-solid fa-download"></i> Xuất dữ liệu
                </button>
                <button class="nor-btn" onclick="openAddPopup()" style="margin-right: 20px"><i
                        class="fa-solid fa-plus"></i> Thêm mới
                </button>
            </div>
        </nav>

        <nav class="nav-bar">
            <div id="searchKeyRequest" style="display: none">${requestScope.searchKey}</div>
            <div class="search-bar-container">
                <input type="text" class="search-bar" id="searchInput"
                       placeholder="Tìm kiếm theo tên thuốc, nhà cung cấp..." required>
                <button id="submit-btn-search" class="search-btn" onclick="submitSearch()" style="float: right"><i
                        class="fa-solid fa-magnifying-glass"></i> Tìm kiếm
                </button>
            </div>
            <div class="search-results-container" id="search-results-container">
            </div>
            <div class="filter-container">
                <c:if test="${requestScope.searchKey != null}">
                    <div class="filter-item"><i class="fa-solid fa-xmark" onclick="window.location = 'item-import'"></i>&nbsp;${requestScope.searchKey}
                    </div>
                </c:if>
                <button class="rev-btn" style="float:right;" onclick="openFilterPopup()"><i
                        class="fa-solid fa-filter"></i> Bộ lọc
                </button>
            </div>
        </nav>

        <nav class="nav-bar table-display">
            <table>
                <thead>
                <tr class="tb-head">
                    <th>No.</th>
                    <th>ID</th>
                    <th>Tên</th>
                    <th>Loại</th>
                    <th>Giá nhập</th>
                    <th>SL</th>
                    <th>Thành tiền</th>
                    <th>Mã NCC</th>
                    <th>Tên Nhà Cung Cấp</th>
                    <th>TT</th>
                    <th>Ngày nhập</th>
                    <th>Tùy chọn</th>
                </tr>
                </thead>
                <tbody class="table-body" id="listItemImport">
                <c:forEach var="itemImport" items="${requestScope.listI}" varStatus="no">
                    <tr>
                        <td>${no.count}</td>
                        <td id="id${itemImport.id}">${itemImport.id}</td>
                        <td id="nameItem${itemImport.id}">${itemDAO.getItemById(itemImport.idItem).nameItem}</td>
                        <td id="type${itemImport.id}">${itemDAO.getItemById(itemImport.idItem).type}</td>
                        <td id="importPrice${itemImport.id}">${itemImport.importPrice}</td>
                        <td id="quantity${itemImport.id}">${itemImport.quantity}</td>
                        <td id="totalPrice${itemImport.id}">${itemImport.totalPrice}</td>
                        <td id="idProvider${itemImport.id}">${itemImport.idProvider}</td>
                        <td id="nameProvider${itemImport.id}">${providerDAO.getProviderById(itemImport.idProvider).name}</td>
                        <c:if test="${itemImport.status == 1}">
                            <td id="status${itemImport.id}">Hợp lệ</td>
                        </c:if>
                        <c:if test="${itemImport.status == 2}">
                            <td id="status${itemImport.id}">Không hợp lệ</td>
                        </c:if>
                        <td id="importDate${itemImport.id}">${itemImport.importDate}</td>
                        <td id="option-btn${itemImport.id}" class="option-btn">
                            <button class="rev-btn" id="showDropdownButton${itemImport.id}"
                                    onclick="showDropDown('${itemImport.id}')">
                                <i class="fa-solid fa-eye"></i></button>
                            <button class="rev-btn" style="display: none" id="closeDropdownButton${itemImport.id}"
                                    onclick="showDropDown('${itemImport.id}')">
                                <i class="fa-solid fa-eye-slash"></i></button>
                            <div id="dropdownContainer${itemImport.id}" class="dropdownBtn">
                                <button onclick="openUpdatePopup(${itemImport.id},
                                    ${itemImport.idItem},
                                        '${itemDAO.getItemById(itemImport.idItem).type}',
                                        '${itemImport.importPrice}',
                                        '${itemImport.quantity}',
                                        '${itemImport.totalPrice}',
                                        '${itemImport.idProvider}',
                                        '${itemImport.importDate}',
                                        '${itemImport.status}'
                                        )">Chỉnh sửa
                                </button>
                                <br>
                                <button onclick="openDeletePopup(${itemImport.id},
                                        '${itemDAO.getItemById(itemImport.idItem).nameItem}',
                                        '${itemImport.importDate}'
                                        )">Xóa
                                </button>
                                <br>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

            <c:if test="${requestScope.listI == null}"><br>
                <h4 style="width: 100%; text-align: center">Hiện chưa có dữ liệu về giao dịch nào, hãy tạo mới</h4>
            </c:if>
        </nav>

        <nav class="nav-bar paging_wrap">
            <!--Pagination-->
            <div class="pagination-btn"
                    <c:if test="${requestScope.page != 1}">
                        onclick="window.location = 'item-import?page=1'"
                        style="color: black"
                    </c:if>class="paging_next hv">
                <i class="fa-solid fa-angles-left"></i>
            </div>
            <c:if test="${requestScope.numP <= 7}">
                <c:forEach begin="1" end="${requestScope.numP}" var="i">
                    <%--                    <div onclick="window.location = 'item-import?page=${i}'"--%>
                    <div onclick="changePage('${i}')"
                         class="paging_page hv <c:if test="${requestScope.page==i}">pagingActive</c:if>">${i}</div>
                </c:forEach>
            </c:if>
            <div class="pagination-btn"
                    <c:if test="${requestScope.page != requestScope.numP}">
                        onclick="window.location = 'item-import?page=${requestScope.numP}'"
                        style="color: black"
                    </c:if>class="paging_next hv">
                <i class="fa-solid fa-angles-right"></i>
            </div>
        </nav>
    </div>

    <div class="overlay" id="popup-delete">
        <div class="popup-delete-content">
            <h2 style="color: red">XÁC NHẬN XÓA?</h2>
            <div class="horizontal-line"></div>
            <form action="item-import" method="post">
                <input type="text" value="" name="itemImportId" id="deleteIdInput" hidden>
                <input type="text" value="delete" name="go" hidden>
                <p>Bạn có chắc chắn muốn xóa giao dịch mã
                    <span id="itemImportId" style="color: #0d6efd"></span>
                    - mặt hàng:
                    <span id="itemImportName" style="color: #0d6efd"></span>
                    - ngày
                    <span id="itemImportDate" style="color: #0d6efd"></span>
                    khỏi hệ thống ko</p>
                <div class="popup-btn-area">
                    <button type="button" class="popup-btn grey-btn" onclick="closeDeletePopup()" style="float:right;">
                        <i
                                class="fa-solid fa-xmark"></i> Hủy
                    </button>
                    <button type="submit" class="popup-btn red-btn" style="float:right;"><i
                            class="fa-solid fa-pen-to-square"></i> Xóa
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="overlay" id="popup-add-import">
        <div class="popup-content">
            <h2>THÊM GIAO DỊCH MỚI</h2>
            <form action="item-import" method="post" onsubmit="return validateForm('add')">
                <input type="text" value="add" name="go" hidden>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="addNameItem">Tên mặt hàng:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <select id="addNameItem" name="idItem" onchange="validateSelect('addNameItem')" required>
                            <option value="0" selected hidden style="color: grey">Chọn thuốc, vật tư</option>
                            <c:forEach var="item" items="${requestScope.listIt}">
                                <option value="${item.id}">${item.nameItem}</option>
                            </c:forEach>
                        </select><br>
                        <div id="errorAddNameItem" class="errorText">Vui lòng chọn một thuốc, vật tư khác</div>
                    </div>
                </div>

                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="addImportPrice">Giá nhập:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <input id="addImportPrice" type="text" value="" name="importPrice" required><span
                            class="currency">VND</span><br>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="addQuantity">Số lượng:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <input id="addQuantity" type="text" value="" name="quantity" required><br>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="addTotalPrice">Thành tiền:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <input id="addTotalPrice" class="readonly-input" type="text" value="" name="totalPrice"
                               readonly><span class="currency">VND</span><br>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="addNameProvider">Nhà cung cấp:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <select id="addNameProvider" name="idProvider" onchange="validateSelect('addNameProvider')"
                                required>
                            <option value="0" selected hidden>Chọn nhà cung cấp</option>
                            <c:forEach var="provider" items="${requestScope.listP}">
                                <option value="${provider.id}">${provider.name}</option>
                            </c:forEach>
                        </select><br>
                        <div id="errorAddNameProvider" class="errorText">Vui lòng chọn một nhà cung cấp khác</div>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="addImportDate">Ngày nhập:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <input id="addImportDate" class="readonly-input" type="text" value="${requestScope.currentDate}"
                               name="importDate"
                               readonly><br>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="addStatus">Trạng thái:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <select id="addStatus" name="status" required>
                            <option value="1" selected>Hợp lệ</option>
                            <option value="2">Không hợp lệ</option>
                        </select><br>
                    </div>
                </div>

                <div class="popup-btn-area">
                    <button type="button" class="popup-btn grey-btn" onclick="closeAddPopup()" style="float:right;"><i
                            class="fa-solid fa-xmark"></i> Hủy
                    </button>
                    <button type="submit" class="popup-btn blue-btn" style="float:right;"><i
                            class="fa-solid fa-pen-to-square"></i> Lưu
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="overlay" id="popup-update-import">
        <div class="popup-content">
            <h2>CHỈNH SỬA GIAO DỊCH</h2>
            <form action="item-import" method="post" onsubmit="return validateForm('update')">
                <input type="text" value="update" name="go" hidden>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="inputID">Mã giao dịch:</label>
                    </div>
                    <div class="col-md-8 form-input">
                        <input id="inputID" class="readonly-input" type="text" value="" name="id" readonly><br>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="inputNameItem">Tên mặt hàng:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <select id="inputNameItem" name="idItem" onchange="validateSelect('inputNameItem')">
                            <option value="0" selected hidden>Chọn thuốc, vật tư</option>
                            <c:forEach var="item" items="${requestScope.listIt}">
                                <option value="${item.id}">${item.nameItem}</option>
                            </c:forEach>
                        </select><br>
                        <div id="errorInputNameItem" class="errorText">Vui lòng chọn một thuốc, vật tư khác</div>
                    </div>
                </div>

                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="inputImportPrice">Giá nhập:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <input id="inputImportPrice" type="text" value="" name="importPrice" required><span
                            class="currency">VND</span><br>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="inputQuantity">Số lượng:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <input id="inputQuantity" type="text" value="" name="quantity" required><br>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="inputTotalPrice">Thành tiền:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <input id="inputTotalPrice" class="readonly-input" type="text" value="" name="totalPrice"
                               readonly><span class="currency">VND</span><br>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="inputNameProvider">Nhà cung cấp:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <select id="inputNameProvider" name="idProvider" onchange="validateSelect('inputNameProvider')"
                                required>
                            <option value="0" selected hidden>Chọn nhà cung cấp</option>
                            <c:forEach var="provider" items="${requestScope.listP}">
                                <option value="${provider.id}">${provider.name}</option>
                            </c:forEach>
                        </select><br>
                        <div id="errorInputNameProvider" class="errorText">Vui lòng chọn một nhà cung cấp khác</div>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="inputImportDate">Ngày nhập:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <input id="inputImportDate" class="readonly-input" type="text" value="" name="importDate"
                               readonly><br>
                    </div>
                </div>
                <div class="row input-section">
                    <div class="col-md-4 form-label">
                        <label for="inputStatus">Trạng thái:</label><br>
                    </div>
                    <div class="col-md-8 form-input">
                        <select id="inputStatus" name="status" required>
                            <option value="1" selected>Hợp lệ</option>
                            <option value="2">Không hợp lệ</option>
                        </select><br>
                    </div>
                </div>

                <div class="popup-btn-area">
                    <button type="button" class="popup-btn grey-btn" onclick="closeUpdatePopup()" style="float:right;">
                        <i
                                class="fa-solid fa-xmark"></i> Hủy
                    </button>
                    <button type="submit" class="popup-btn blue-btn" style="float:right;"><i
                            class="fa-solid fa-pen-to-square"></i> Lưu
                    </button>

                </div>
            </form>
        </div>
    </div>

    <div id="filter-popup" class="filter-popup">
        <div class="filter-popup-content">
            <nav class="filter-nav">
                <div style="float:left;"><h3>Bộ lọc tìm kiếm</h3></div>
                <span onclick="closeFilterPopup()" style="float: right" class="filter-close-btn"><i
                        class="fa-solid fa-xmark"></i></span>
            </nav>
            <nav class="filter-nav">
                <h5>Ngày nhập</h5>
                <div class="horizontal-line"></div>
                <label for="datepicker">Chọn ngày tháng:</label>
                <div class="row">
                    <div class="col-md-8 ">
                        <input class="filter-input" type="text"></div>
                    <div class="col-md-3" onclick="datePicker()">
                        <i class="fa-solid fa-calendar-days"></i>
                    </div>
                </div>

            </nav>

            <nav class="filter-nav">
                <h5>Trạng thái</h5>
                <div class="horizontal-line"></div>
            </nav>

            <nav class="filter-nav">
                <h5>Loại</h5>
                <div class="horizontal-line"></div>
            </nav>
        </div>
    </div>
</div>
</body>
<script src="<%=request.getContextPath()%>/js/item-import.js"></script>

</html>
