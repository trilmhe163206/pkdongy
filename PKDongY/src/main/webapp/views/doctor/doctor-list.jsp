<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 12/10/2023
  Time: 10:33 am
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Doctor-List</title>
    <style>
        div.wrap-page {
            margin-top: -15px;
        }

        .content-page {
            box-shadow: none !important;
            margin: 0 !important;
            padding-top: 0 !important;
        }
    </style>
    <%@include file="../template/import.jsp" %>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/content.css">
</head>
<body>
<div class="wrap wrap-page">
    <jsp:include page="../template/sidebar.jsp"/>
    <div class="content-page">
        <jsp:include page="content.jsp"></jsp:include>
    </div>
</div>
</body>
</html>
