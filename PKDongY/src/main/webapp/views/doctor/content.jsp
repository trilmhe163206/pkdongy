<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 13/10/2023
  Time: 10:23 am
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <meta charset="utf-8"/>
    <style>
        .p-3.m-0.border-0.bd-example.m-0.border-0 {
            padding: 15px 0 0 !important;
        }


        .container {
            flex: 1;
            margin: 10px;
            border-radius: var(--borderRad);
            border-color: var(--borderBtnColor);
            box-shadow: var(--boxShadow);
            max-height: max-content;
            padding: 10px;
        }

        /*Paging*/
        .paging_wrap {
            /*margin-top: -360px;*/
            padding: 10px;
            color: black;
        }

        .paging_wrap div {
            padding: 6px 12px;
            margin-right: 4px;
            font-size: 16px;
            font-weight: 600;
        }

        .paging_wrap .hv:hover {
            cursor: pointer;
            border: 1px solid #91ABED;
            border-radius: 6px;
        }

        .paging_wrap {
            display: flex;
            justify-content: center;

        }

        .pagingActive {
            background-color: #285BC4;
            color: white;
            border-radius: 6px;
            cursor: default;
        }

    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
    />
    <link
            href="https://getbootstrap.com/docs/5.3/assets/css/docs.css"
            rel="stylesheet"
    />
    <title>doctor-list</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

</head>
<body class="p-3 m-0 border-0 bd-example m-0 border-0">
<!-- Example Code -->
<div class="container">
    <nav class="nav-bar">
        <div class="page-name">
            <h2>QUẢN LÝ BÁC SỸ</h2>

        </div>
        <div class="button-area" style="float: right">

            <button class="nor-btn" style="margin-right: 20px"
                    onclick="window.location = '<%= request.getContextPath() %>/doctor-add'"><i
                    class="fa-solid fa-plus"></i> Thêm mới
            </button>
        </div>
    </nav>
    <form action="doctor-list" method="get">
        <nav class="nav-bar">
            <div class="search-bar-container">
                <input type="text" name="search-information" class="search-bar" placeholder="Nhập từ khóa tìm kiếm..."
                       value="${search}">
                <button type="submit" class="search-btn" style="float: right"><i
                        class="fa-solid fa-magnifying-glass"></i> Tìm kiếm
                </button>
            </div>
            <div class="filter-container" style="display: flex; margin-left: 800px">
                <input class="checkbox-label" type="checkbox" id="working" name="status" value="1"
                       <c:if test="${status == '1' || status == 1}">checked</c:if>>
                <label for="working" style="    margin-right: 10px; margin-top: 5px;"> Đang làm việc</label><br>
                <input class="checkbox-label" type="checkbox" id="has-retired" name="status" value="0"
                <c:choose>
                       <c:when test="${status == '0' || status == 0}">checked</c:when>
                    <c:otherwise></c:otherwise>
                </c:choose>>
                <label for="has-retired" style="    margin-right: 10px; margin-top: 5px;"> Đã nghỉ việc</label><br>
                <button class="rev-btn" type="submit" style="float:right;"><i class="fa-solid fa-filter"></i> Lọc
                </button>
            </div>
        </nav>
    </form>

    <div class="row">
        <c:forEach items="${listD}" var="listD">
            <div class="col-md-6 mb-3 card-link">
                <div class="card" style="cursor: pointer" onclick="handleCardClick(event,'${listD.getIdDoctor()}')">

                    <div class=" row g-0">
                        <div class="col-md-3">
                            <svg
                                    class="bd-placeholder-img img-fluid rounded-start"
                                    width="100%"
                                    height="160"
                                    xmlns="http://www.w3.org/2000/svg"
                                    role="img"
                                    aria-label="Placeholder: Image"
                                    preserveAspectRatio="xMidYMid slice"
                                    focusable="false"
                            >
                                <title>Placeholder</title>
                                <c:choose>
                                    <c:when test="${not empty listD.getAvatar_path()}">
                                        <image xlink:href="<%= request.getContextPath() %>${listD.getAvatar_path()}"
                                               width="100%"
                                               height="100%" fill="#868e96"></image>
                                    </c:when>
                                    <c:otherwise>
                                        <rect width="100%" height="100%" fill="#868e96"></rect>
                                        <text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image</text>
                                    </c:otherwise>
                                </c:choose>


                            </svg>
                        </div>
                        <div class="col-md-9" style="height: 160px;">
                            <div class="card-body">
                                <h5 class="card-title">${listD.getNameDoctor()}
                                    <c:if
                                            test="${!listD.status}"><i style="float: right"
                                                                       class="fa-solid fa-trash"
                                                                       onclick="openDeletePopup(event,${listD.getIdDoctor()}, '${listD.getNameDoctor()}')"
                                    ></i></c:if>
                                </h5>
                                <p class="card-text">
                                    <small class="text-body-secondary"
                                    ><c:if test="${listD.status}">Đang làm việc</c:if> <c:if
                                            test="${!listD.status}">Đã nghỉ việc</c:if></small
                                    >
                                </p>
                                <div class="row g-0">

                                    <p class="col-md-6" style="height: 30px">
                                        <i class="fa-solid fa-user-doctor"></i> ${listD.getTitleDoctor()}<br>
                                        <i class="fa-solid fa-user-graduate"></i> ${listD.getAcademicRank()}
                                    </p>
                                    <p class="col-md-6">
                                        <c:set var="originalText"
                                               value="${listD.getDescriptionDoctor()}"/>
                                        <c:choose>
                                            <c:when test="${originalText.length()>=40}">
                                                <c:set var="shortenedText"
                                                       value="${fn:substring(originalText, 0, 60)}"/>
                                                ${shortenedText}<span style="color: #0d6efd">...Xem thêm</span>
                                            </c:when>
                                            <c:otherwise>
                                                ${originalText}
                                            </c:otherwise>
                                        </c:choose>
                                    </p>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>

    </div>
    <div>
        <!--Pagination-->
        <div class="paging_wrap">
            <div class="paging_previous" style="display: <c:if test="${currentPage != 1}">none</c:if>; cursor: default">
                <i class="fa-solid fa-angles-left" style="color: #d0d7de"></i>
            </div>
            <div onclick="window.location = 'doctor-list?page=${currentPage-1}&search-information=${search}&status=${status}'"
                 class="paging_previous hv"
                 style="display: <c:if test="${currentPage == 1}">none</c:if>">
                <i class="fa-solid fa-angles-left"></i>
            </div>
            <c:forEach begin="1" end="${totalPage}" var="i">
                <div onclick="window.location = 'doctor-list?page=${i}&search-information=${search}&status=${status}'"
                     class="paging_page hv <c:if test="${currentPage==i}">pagingActive</c:if>">${i}</div>
            </c:forEach>
            <div onclick="window.location = 'doctor-list?page=${currentPage+1}&search-information=${search}&status=${status}'"
                 class="paging_next hv"
                 style="display: <c:if test="${currentPage == totalPage}">none</c:if>">
                <i class="fa-solid fa-angles-right"></i>
            </div>
            <div class="paging_previous" style="display: <c:if test="${currentPage != totalPage}">none</c:if>">
                <i class="fa-solid fa-angles-right" style="color: #d0d7de"></i>
            </div>
        </div>
    </div>
    <div class="overlay" id="popup-delete">
        <div class="popup-delete-content">
            <h2 style="color: red">XÁC NHẬN XÓA?</h2>
            <div class="horizontal-line"></div>
            <input type="text" value="" name="itemImportId" id="deleteIdInput" hidden>
            <input type="text" value="delete" name="go" hidden>
            <p>Bạn có chắc chắn muốn xóa Bác sĩ
                <span id="itemImportName" style="color: #0d6efd"></span>
                - Mã:
                <span id="itemImportId" style="color: #0d6efd"></span>
                khỏi hệ thống ko</p>
            <div class="popup-btn-area">
                <button type="button" class="popup-btn grey-btn" onclick="closeDeletePopup()" style="float:right;">
                    <i
                            class="fa-solid fa-xmark"></i> Hủy
                </button>
                <button type="submit" class="popup-btn red-btn" style="float:right;"
                        onclick="redirectToDoctorDelete()"><i
                        class="fa-solid fa-pen-to-square"></i> Xóa
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    const workingCheckbox = document.getElementById("working");
    const retiredCheckbox = document.getElementById("has-retired");

    workingCheckbox.addEventListener("click", function () {
        if (workingCheckbox.checked) {
            retiredCheckbox.checked = false;
        }
    });

    retiredCheckbox.addEventListener("click", function () {
        if (retiredCheckbox.checked) {
            workingCheckbox.checked = false;
        }
    });

    function handleCardClick(event, doctorID) {
        const clickedElement = event.target;
        if (clickedElement.tagName === 'I' && clickedElement.classList.contains('fa-trash')) {
            event.stopPropagation();
        } else {
            var url = '<%= request.getContextPath() %>/doctor-detail?doctorID=' + doctorID;
            window.location.href = url;
        }
    }

    function redirectToDoctorDetail(doctorID) {
        var url = '<%= request.getContextPath() %>/doctor-detail?doctorID=' + doctorID;
        window.location.href = url;
    }

    function openDeletePopup(event, id, name) {
        document.getElementById("itemImportId").innerText = id;
        document.getElementById("itemImportName").innerText = name;
        document.getElementById("deleteIdInput").value = id;

        document.getElementById("popup-delete").style.display = "flex";
        event.stopPropagation();
    }

    function closeDeletePopup() {
        document.getElementById("popup-delete").style.display = "none";
    }

    function redirectToDoctorDelete() {
        var doctorID = document.getElementById("deleteIdInput").value;
        var url = '<%= request.getContextPath() %>/doctor-detail?delete=1&doctorID=' + doctorID;
        window.location.href = url;
    }

    // document.addEventListener("DOMContentLoaded", function () {
    //     var cards = document.querySelectorAll(".card-link");
    //
    //
    //     cards.forEach(function (card) {
    //         card.addEventListener("click", function () {
    //             window.location.href = "doctor-detail?";
    //         });
    //     });
    // });
</script>
</body>
</html>
