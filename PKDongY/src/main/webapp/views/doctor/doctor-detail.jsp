<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 09/11/2023
  Time: 10:38 am
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Detail doctor</title>
    <%@include file="../template/import.jsp" %>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/content.css">
    <style>
        .p-3.m-0.border-0.bd-example.m-0.border-0 {
            padding: 15px 0 0 !important;
        }

        div.wrap-page {
            margin-top: -15px;
        }

        .avatar-doctor {
            height: 180px;
            width: 180px;
            border-radius: 0 20px 20px 20px;
            float: left;
            padding: 10px;
            border: 2px solid var(--topBarColor);
            margin-bottom: 5px;
        }

        .col-custom-1 {
            flex: 0 0 4.16%; /* 100% / 24 */
            max-width: 4.16%;
        }

        .phan-cach {
            margin-bottom: 5px !important;
            margin-top: 5px !important;
        }


    </style>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
    />
    <link
            href="https://getbootstrap.com/docs/5.3/assets/css/docs.css"
            rel="stylesheet"
    />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body class="p-3 m-0 border-0 bd-example m-0 border-0">
<div class="wrap wrap-page">
    <jsp:include page="../template/sidebar.jsp"/>
    <div class="content-page">
        <div class="container">

            <div class="row">
                <div class="col-custom-1">
                    <button onclick="window.location = '<%= request.getContextPath() %>/doctor-list'" style=" background-color: #285BC4;
                        border: 2px solid #285BC4;color: white; border-radius: 15px; height: 26px; width: 60px; font-size: 12px;
                ">Quay lại
                    </button>
                </div>
                <div class="col-md-11" style="margin-top: 40px; margin-left: -50px">
                    <nav class="nav-bar">
                        <div class="page-name">
                            <h2>THÔNG TIN CHI TIẾT</h2>

                        </div>
                        <div class="button-area" style="float: right">

                            <button class="nor-btn" style="margin-right: 20px; margin-top: 10px;"
                                    onclick="window.location = '<%= request.getContextPath() %>/doctor-edit?doctorID=${doctorID}'">
                                <i
                                        class="fa-regular fa-pen-to-square"></i> Chỉnh sửa
                            </button>
                        </div>
                        <hr style="margin-top: 60px;">
                    </nav>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="<%= request.getContextPath() %>${doctor.getAvatar_path()}" class="avatar-doctor">
                        </div>
                        <div class="col-md-9">
                            <label>
                                <input name="doctorID" value="${doctorID}" style="display: none">
                            </label>
                            <span>Họ và tên: ${doctor.getNameDoctor()}</span>
                            <hr class="phan-cach">
                            <span>Trạng thái: <c:if test="${doctor.status}">Đang làm việc</c:if> <c:if
                                    test="${!doctor.status}">Đã nghỉ việc</c:if></span>
                            <hr class="phan-cach">
                            <span>Địa chỉ: ${doctor.getAddress()}</span>
                            <hr class="phan-cach">
                            <span>Chức danh: ${doctor.getTitleDoctor()}</span>
                            <hr class="phan-cach">
                            <span style="border-right: 1px solid black;width: 370px;display: inline-block;">Học hàm: ${doctor.getAcademicRank()}</span>
                            <span>Học vị: ${doctor.getDegree()}</span>
                            <hr class="phan-cach">
                            <span>Số điện thoại: ${doctor.getPhone()}</span>
                            <hr class="phan-cach">
                            <span>Tên đăng nhập: ${doctor.getUsername()}</span>
                            <hr class="phan-cach">
                            <span>Mật khẩu: ${doctor.getPassword()}</span>
                            <hr class="phan-cach">
                            <span>Mô tả: ${doctor.getDescriptionDoctor()}</span>
                            <hr class="phan-cach">
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<script>

</script>
</body>
</html>

