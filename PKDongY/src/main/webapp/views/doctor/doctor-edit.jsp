<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 09/11/2023
  Time: 10:39 am
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit doctor</title>
    <%@include file="../template/import.jsp" %>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/content.css">
    <style>
        .p-3.m-0.border-0.bd-example.m-0.border-0 {
            padding: 15px 0 0 !important;
        }

        div.wrap-page {
            margin-top: -15px;
        }

        .form-input input {
            width: 90% !important;
        }

        .form-input textarea {
            width: 90%;
            border: 1px solid var(--formBorderColor);
            /*box-shadow: var(--boxShadow);*/
            border-radius: var(--btnRad);
            margin-bottom: 5px;
            height: 150px;
            background-color: var(--formInputColor);
            padding: 0 10px;
        }

        .header-add-doctor h2 {
            font-family: 'Inter', sans-serif;
            text-align: center;
            margin-bottom: 30px;
        }

    </style>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
    />
    <link
            href="https://getbootstrap.com/docs/5.3/assets/css/docs.css"
            rel="stylesheet"
    />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body class="p-3 m-0 border-0 bd-example m-0 border-0">
<div class="wrap wrap-page">
    <jsp:include page="../template/sidebar.jsp"/>
    <div class="content-page">
        <div class="container">
            <div class="header-add-doctor">
                <button onclick="window.location = '<%= request.getContextPath() %>/doctor-list'" style=" background-color: #285BC4;
                        border: 2px solid #285BC4;color: white; border-radius: 15px
                ">Quay lại
                </button>
                <h2>CHỈNH SỬA BÁC SĨ</h2>

            </div>
            <form action="doctor-edit" method="post" enctype="multipart/form-data">
                <label>
                    <input name="doctorID" value="${doctorID}" style="display: none">
                </label>
                <div class="row">

                    <div class="col-md-9 input-form">
                        <div class="row input-section">
                            <div class="col-md-2 form-label">
                                Họ và tên
                            </div>
                            <div class="col-md-10 form-input">
                                <input id="addDoctorName" type="text" value="${doctor.getNameDoctor()}"
                                       name="importName"><br>
                            </div>
                        </div>

                        <div class="row input-section">
                            <div class="col-md-2 form-label">
                                Địa chỉ
                            </div>
                            <div class="col-md-10 form-input">
                                <input id="addAddress" type="text" value="${doctor.getAddress()}"
                                       name="importAddress"><br>
                            </div>
                        </div>

                        <div class="row input-section">
                            <div class="col-md-2 form-label">
                                Chức danh
                            </div>
                            <div class="col-md-10 form-input">
                                <input id="addTitle" type="text" value="${doctor.getTitleDoctor()}"
                                       name="importTitle"><br>
                            </div>
                        </div>

                        <div class="row input-section">
                            <div class="col-md-2 form-label">
                                Học hàm
                            </div>
                            <div class="col-md-10 form-input">
                                <input id="addAcaRank" type="text" value="${doctor.getAcademicRank()}"
                                       name="importAcaRank"><br>
                            </div>
                        </div>

                        <div class="row input-section">
                            <div class="col-md-2 form-label">
                                Học vị
                            </div>
                            <div class="col-md-10 form-input">
                                <input id="addDegree" type="text" value="${doctor.getDegree()}" name="importDegree"><br>
                            </div>
                        </div>

                        <div class="row input-section">
                            <div class="col-md-2 form-label">
                                Số điện thoại
                            </div>
                            <div class="col-md-10 form-input">
                                <input id="addPhone" type="text" value="${doctor.getPhone()}"
                                       name="importPhone"><br>
                            </div>
                        </div>

                        <div class="row input-section">
                            <div class="col-md-2 form-label">
                                Tên đăng nhập
                            </div>
                            <div class="col-md-10 form-input">
                                <input id="addUsername" type="text" value="${doctor.getUsername()}"
                                       name="importUsername"><br>
                            </div>
                        </div>

                        <div class="row input-section">
                            <div class="col-md-2 form-label">
                                Mật khẩu
                            </div>
                            <div class="col-md-10 form-input">
                                <input id="addPassword" type="text" value="${doctor.getPassword()}"
                                       name="importPassword"><br>
                            </div>
                        </div>
                        <div class="row input-section">
                            <div class="col-md-2 form-label">
                                Mô tả
                            </div>
                            <div class="col-md-10 form-input">
                                <textarea id="addDescrip" type="text" value=""
                                          name="importDescrip">${doctor.getDescriptionDoctor()}</textarea><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 avatar-form">

                        <img id="previewImage" src="<%= request.getContextPath() %>${doctor.getAvatar_path()}"
                             alt="Ảnh đại diện
"
                             style="height: 220px; width: 165px; margin-left: 60px;">
                        <button type="button"
                                style="display:block;width:120px; height:30px; margin-left: 85px; margin-top: 10px;
                                border-radius: 15px;border: 2px solid whitesmoke;"
                                onclick="document.getElementById('myFile').click()"><i class="fa-regular fa-image"></i>Chỉnh
                            sửa
                        </button>
                        <input type='file' id="myFile" style="display:none" accept=".jpg, .png, .gif" name="filename">
                        <%--                        <input type="file" id="myFile1" accept=".jpg, .png, .gif" name="filename">--%>

                        <div class="more-information" style="margin-left: 20px">
                            <input class="checkbox-label" type="checkbox" id="working" name="status" value="1"
                                   <c:if test="${doctor.isStatus()}">checked</c:if>>
                            <label for="working" style="    margin-right: 10px; margin-top: 5px;"> Đang làm việc</label><br>
                            <input class="checkbox-label" type="checkbox" id="has-retired" name="status" value="0"
                            <c:choose>
                                   <c:when test="${!doctor.isStatus()}">checked</c:when>
                                <c:otherwise></c:otherwise>
                            </c:choose>>
                            <label for="has-retired" style="    margin-right: 10px; margin-top: 5px;"> Đã nghỉ
                                việc</label><br>
                            <label>Ngày tạo:</label>${doctor.getCreateDate()}<br>
                            <label>Ngày cập nhật:</label>${doctor.getUpdateDate()}
                        </div>


                    </div>


                </div>
                <div class="button-submit">
                    <button type="submit" style="background-color: #285BC4; float: inherit;margin-top: 25px; margin-left: 510px;height: 30px;width: 120px;
                    border: 2px solid #285BC4;color: white; border-radius: 15px">Lưu
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>
<script>
    var previewImage = document.getElementById('previewImage');
    previewImage.dataset.originalSrc = previewImage.src;
    document.getElementById("myFile").onchange = function (event) {
        var input = event.target;
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function () {
                var dataURL = reader.result;
                var previewImage = document.getElementById('previewImage');
                previewImage.src = dataURL;
                previewImage.style.display = 'block';
            };

            reader.readAsDataURL(input.files[0]);
        } else {
            // If user dont choose file, keep the doctor's avatar path
            var previewImage = document.getElementById('previewImage');
            previewImage.src = previewImage.dataset.originalSrc;
            previewImage.style.display = 'block';
        }
    };

    <%--document.getElementById("myFile").onchange = function (event) {--%>
    <%--    var input = event.target;--%>
    <%--    if (input.files && input.files[0]) {--%>
    <%--        var reader = new FileReader();--%>

    <%--        reader.onload = function () {--%>
    <%--            var dataURL = reader.result;--%>
    <%--            var previewImage = document.getElementById('previewImage');--%>
    <%--            previewImage.src = dataURL;--%>
    <%--            previewImage.style.display = 'block';--%>
    <%--        };--%>

    <%--        reader.readAsDataURL(input.files[0]);--%>
    <%--    } else {--%>
    <%--        // If user dont choose file, keep default file--%>
    <%--        var previewImage = document.getElementById('previewImage');--%>
    <%--        previewImage.src = '<%= request.getContextPath() %>/img/Avatar.png';--%>
    <%--        previewImage.style.display = 'block';--%>
    <%--    }--%>
    <%--};--%>

    //check tick working
    const workingCheckbox = document.getElementById("working");
    const retiredCheckbox = document.getElementById("has-retired");

    workingCheckbox.addEventListener("click", function () {
        if (workingCheckbox.checked) {
            retiredCheckbox.checked = false;
        }
    });

    retiredCheckbox.addEventListener("click", function () {
        if (retiredCheckbox.checked) {
            workingCheckbox.checked = false;
        }
    });
    //Check input information
    document.addEventListener("DOMContentLoaded", function () {
        // Lấy form và các trường input và textarea
        var form = document.querySelector("form");
        var addDoctorName = document.getElementById("addDoctorName");
        var addAddress = document.getElementById("addAddress");
        var addTitle = document.getElementById("addTitle");
        var addAcaRank = document.getElementById("addAcaRank");
        var addDegree = document.getElementById("addDegree");
        var addPhone = document.getElementById("addPhone");
        var addUsername = document.getElementById("addUsername");
        var addPassword = document.getElementById("addPassword");
        var addDescrip = document.getElementById("addDescrip");


        form.addEventListener("submit", function (event) {
            if (
                !addDoctorName.value.trim() ||
                !addAddress.value.trim() ||
                !addTitle.value.trim() ||
                !addAcaRank.value.trim() ||
                !addDegree.value.trim() ||
                !addPhone.value.trim() ||
                !addUsername.value.trim() ||
                !addPassword.value.trim() ||
                !addDescrip.value.trim()
            ) {
                event.preventDefault();
                alert("Vui lòng điền đầy đủ thông tin.");
            } else {
            }
        });
    });


</script>
</body>
</html>
