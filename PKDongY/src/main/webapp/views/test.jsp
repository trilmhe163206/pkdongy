<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 10/16/2023
  Time: 3:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Test Page</title>
    <%@include file="template/import.jsp" %>
</head>
<body>
<div class="wrap wrap-page">
    <jsp:include page="template/sidebar.jsp"/>
    <div class="content-page">
        <div>
            <p>Day la content test page</p>
        </div>
    </div>
</div>
</body>
</html>
