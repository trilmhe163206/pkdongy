<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 10/16/2023
  Time: 3:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home Page</title>
    <%@include file="template/import.jsp" %>
    <style>
        <%@include file="../css/home.css" %>
    </style>
</head>
<body>
<div class="wrap wrap-page">
    <jsp:include page="template/sidebar.jsp"/>
    <div class="content-page"
         style="background-image: url('../img/anhnen2.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center">
        <%--<div class="content-page">--%>
        <div class="home-content">
            <c:if test="${sessionScope.clinic != null}">
                <div class="overlay home-overlay col">
                    <h1 class="row-md-2 hc-row">Quản lý ${sessionScope.clinic.name}</h1><br>
                    <div class="row-md-4 hc-row">
                        <h3> ${sessionScope.clinic.description}
                        </h3>
                        <br><h4> Đại diện: Bác sĩ ${sessionScope.clinic.representative}</h4>
                    </div>

                    <c:if test="${sessionScope.isLoggedIn != true}">
                        <h6 class="row-md-1 hc-row"></h6>
                        <div class="row-md-4 hc-row">
                            <button class="home-btn">
                                <a href="login">Đăng nhập</a>
                            </button>
                        </div>
                    </c:if>

                    <div class="row-md-1 hc-row">
                        <h5>Liên hệ: ${sessionScope.clinic.phone}</h5>
                        <h5>Địa chỉ: ${sessionScope.clinic.address}</h5>
                    </div>
                    </h6>
                </div>
            </c:if>
            <c:if test="${sessionScope.clinic == null}">
                <div class="overlay home-overlay col">
                    <h1 class="row-md-2 hc-row">Phần mềm quản lý phòng khám Đông Y</h1><br>

                    <c:if test="${sessionScope.isLoggedIn != true}">
                        <div class="row-md-4 hc-row">
                            <br><h4> Hiện tại chưa có thông tin phòng khám, hãy đăng nhập để cập nhật thông tin</h4>
                        </div>
                        <h6 class="row-md-1 hc-row"></h6>
                        <div class="row-md-4 hc-row">
                            <button class="home-btn">
                                <a href="login">Đăng nhập</a>
                            </button>
                        </div>
                    </c:if>

                    <c:if test="${sessionScope.isLoggedIn == true}">
                        <div class="row-md-4 hc-row">
                            <br><h4> Hiện tại chưa có thông tin phòng khám</h4>
                        </div>
                        <h6 class="row-md-1 hc-row"></h6>
                        <div class="row-md-4 hc-row">
                            <button class="home-btn">
                                Cập nhật thông tin
                            </button>
                        </div>
                    </c:if>
                </div>
            </c:if>
        </div>
    </div>
</body>
</html>
