<%--<link rel="icon" href="<%=request.getContextPath()%>/resource/logo-removebg.png" type="image/x-icon">--%>
<%--<link rel="shortcut icon" type="image/x-icon" href="<%=request.getContextPath()%>/resource/logo-removebg.png">--%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/common.css">
<%--<link rel="stylesheet" href="<%=request.getContextPath()%>/css/content.css">--%>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.4.0/css/all.min.css">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet"/>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"   %>