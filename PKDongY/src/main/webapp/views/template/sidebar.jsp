<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 12/10/2023
  Time: 10:36 am
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<%= request.getContextPath() %>/css/common.css">
    <link rel="stylesheet" href="<%= request.getContextPath() %>/css/sidebar.css">

</head>
<body>
<%--<div class="wrap wrap-top-bar">--%>
<%--    <div class="top-bar">--%>
<%--        <i class="fa-solid fa-bars fa-icon"></i>--%>
<%--    </div>--%>
<%--    <div class="menu-drop-list">--%>
<%--        <div class="sb-content user-info">--%>
<%--            <nav class="user-info-content">--%>
<%--                <img class="AvatarDefault"--%>
<%--                     src="../image/Avatar.png" alt="avatar-default"/>--%>
<%--            </nav>--%>
<%--            <nav class="user-info-content"><h5 class="sb-username">Quản Trị Viên</h5></nav>--%>
<%--        </div>--%>
<%--        <div class="sb-content function active">--%>
<%--            <a href="#patient">Quản lý bệnh nhân</a>--%>
<%--        </div>--%>
<%--        <div class="sb-content function">--%>
<%--            <a href="#item">Quản lý thuốc/vật tư</a>--%>
<%--        </div>--%>
<%--        <div class="sb-content function">--%>
<%--            <a href="#import_item">Nhập thuốc/vật tư</a>--%>
<%--        </div>--%>
<%--        <div class="sb-content function">--%>
<%--            <a class="active" href="#doctor">Quản lý bác sĩ</a>--%>
<%--        </div>--%>
<%--        <div class="sb-content function">--%>
<%--            <a href="#provider">Quản lý nhà cung cấp</a>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</div>--%>

<div class="sidebar">
    <div class="user-info">
        <div class="avatar">
            <img class="AvatarDefault"
                 src="<%= request.getContextPath() %>/img/Avatar.png" alt="avatar-default"/>
        </div>
        <c:if test="${sessionScope.isLoggedIn == true}">
            <div>

                <c:if test="${sessionScope.role == 'admin'}">
                    <h4 class="sb-username">Quản Trị Viên</h4>
                </c:if>
                <c:if test="${sessionScope.role == 'doctor'}">
                    <h4 class="sb-username">${sessionScope.account.username}</h4>
                </c:if>
                <div>
                    <button class="sb-btn" style="float: left; font-size: 10px">
                        <a href="login?go=logout">Đăng xuất</a></button>
                </div>
            </div>
        </c:if>

        <c:if test="${sessionScope.isLoggedIn != true}">
            <div>
                <h5 class="sb-username">Bạn chưa đăng nhập</h5>
                <div>
                    <button class="sb-btn" style="float: left; font-size: 10px">
                        <a href="login">Đăng nhập</a>
                    </button>
                </div>
            </div>

        </c:if>

    </div>
    <div class="sb-content <c:if test="${requestScope.currentPage1 == null   }"> active</c:if>
<c:if test="${requestScope.currentPage1 == 'home'}"> active</c:if>">
        <a href="home">Trang chủ</a>
    </div>
    <c:if test="${sessionScope.isLoggedIn == true}">
        <div class="sb-content">
            <a href="#patient">Quản lý bệnh nhân</a>
        </div>
        <div class="sb-content">
            <a href="#item">Quản lý thuốc/vật tư</a>
        </div>
        <div class="sb-content <c:if test="${requestScope.currentPage1 == 'itemImport'}"> active</c:if>">
            <a href="item-import">Nhập thuốc/vật tư</a>
        </div>
        <div class="sb-content <c:if test="${currentPage1 == 'doctor'}"> active</c:if>">
            <a href="doctor-list">Quản lý bác
                sĩ</a>
        </div>
        <div class="sb-content ">
            <a href="#provider">Quản lý nhà cung cấp</a>
        </div>
    </c:if>
    <div class="last-sb">

    </div>
</div>
</body>
</html>

