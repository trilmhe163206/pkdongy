<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 11/10/2023
  Time: 10:30 am
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login</title>
    <style>
        .khung {
            margin-top: 60px;
        }

        .all {
            border: 1px solid white;
            width: 640px;
            height: 570px;
            background-color: #ffffff;
            border-radius: 2%;
            margin-top: 100px
        }

        .form-input {
            width: 380px;
            height: 44px;
            border-color: #00000000;
            font-size: 17px;
            background-color: #0000000d;
            font-family: 'Montserrat', Arial, Helvetica, sans-serif;
        }

        .submit {
            width: 250px;
            height: 40px;
            font-size: 20px;
            border-radius: 24px;
            background-color: #68eedfab;
            font-family: 'Montserrat', Arial, Helvetica, sans-serif;
            font-weight: 700;
            color: white;
            border-color: #00000000;
            margin-top: 40px;
            transition: background-color 0.3s ease;
        }

        .submit:hover {
            background-color: #3aa7e7ab;
        }

        .h1 {
            text-align: center;
            font-family: 'Montserrat', Arial, Helvetica, sans-serif;
            margin-top: 30px;
        }
    </style>
</head>
<body style="background-image: url('<%= request.getContextPath() %>/img/anhnen1.png') ;background-size: 100%">
<div style="display: flex ; justify-content: center;align-items: center;height: 600px">
    <div class="all">
        <h1 class="h1">Đăng nhập</h1>
        <h2 style="text-align: center;" class="h2">Phòng khám Đông Y</h2>
        <div class="khung">
            <h3 style="margin-left: 125px;font-family: 'Montserrat',Arial,Helvetica,sans-serif">Tên đăng nhập </h3>
            <form action="login" method="post">
                <div class="form-group" style="text-align: center ">
                    <input type="text" class="form-input" placeholder="    Tên đăng nhập" id="username"
                           value="${username}"
                           name="username">
                </div>
                <div class="mid_register" style="display: inline-flex">
                    <h3 style="margin-left: 125px;font-family: 'Montserrat',Arial,Helvetica,sans-serif">Mật khẩu </h3>
                </div>
                <div class="form-group" style="text-align: center ">
                    <input type="password" class="form-input" placeholder="    Mật khẩu" value="${password}"
                           id="password" name="password">
                </div>
                <div style="color: red;text-align: center  ">
                    <h4>  ${msgError}</h4>
                </div>

                <br>
                <div style="text-align: center">
                    <input style="cursor: pointer" value="Đăng nhập" class="submit" type="submit" name="registerBt"/>
                </div>
            </form>
            <br>
        </div>
    </div>
</div>
</body>
</html>
