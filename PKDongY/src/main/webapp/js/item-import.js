function openUpdatePopup(id, idItem, type, importPrice, quantity, totalPrice, idProvider, importDate, status) {
    document.getElementById("inputID").value = id;
    document.getElementById("inputNameItem").value = idItem;
    document.getElementById("inputImportPrice").value = importPrice;
    document.getElementById("inputQuantity").value = quantity;
    document.getElementById("inputTotalPrice").value = totalPrice;
    document.getElementById("inputNameProvider").value = idProvider;
    document.getElementById("inputImportDate").value = importDate;
    document.getElementById("inputStatus").value = status;
    document.getElementById("popup-update-import").style.display = "flex";
}

function openAddPopup() {
    document.getElementById("popup-add-import").style.display = "flex";
}

function closeAddPopup() {
    document.getElementById("popup-add-import").style.display = "none";
}

function closeUpdatePopup() {
    document.getElementById("popup-update-import").style.display = "none";
}

function openDeletePopup(id, name, date) {
    document.getElementById("itemImportId").innerText = id;
    document.getElementById("itemImportName").innerText = name;
    document.getElementById("itemImportDate").innerText = date;
    document.getElementById("deleteIdInput").value = id;

    document.getElementById("popup-delete").style.display = "flex";
}

function closeDeletePopup() {
    document.getElementById("popup-delete").style.display = "none";
}

function openFilterPopup() {
    document.getElementById("filter-popup").style.display = "block";
}

function closeFilterPopup() {
    document.getElementById("filter-popup").style.display = "none";
}

const inputImportPrice = document.getElementById("inputImportPrice");
const inputQuantity = document.getElementById("inputQuantity");
const addImportPrice = document.getElementById("addImportPrice");
const addQuantity = document.getElementById("addQuantity");
const inputNameItem = document.getElementById("inputNameItem");
const tongTienSpan = document.getElementById("tongTien");

inputImportPrice.addEventListener("input", calcTotalPrice);
inputQuantity.addEventListener("input", calcTotalPrice);
addImportPrice.addEventListener("input", calcTotalAddPrice);
addQuantity.addEventListener("input", calcTotalAddPrice);

function calcTotalPrice() {
    const importPrice = inputImportPrice.value;
    const quantity = inputQuantity.value;

    document.getElementById("inputTotalPrice").value = importPrice * quantity;
}

function calcTotalAddPrice() {
    const importPrice = addImportPrice.value;
    const quantity = addQuantity.value;

    document.getElementById("addTotalPrice").value = importPrice * quantity;
}

inputImportPrice.addEventListener("input", function (event) {
    this.value = this.value.replace(/[^0-9]/g, '');
});

addImportPrice.addEventListener("input", function (event) {
    this.value = this.value.replace(/[^0-9]/g, '');
});

inputQuantity.addEventListener("input", function (event) {
    this.value = this.value.replace(/[^0-9]/g, '');
});

addQuantity.addEventListener("input", function (event) {
    this.value = this.value.replace(/[^0-9]/g, '');
});

function validateForm(go) {
    if (go === "add") {
        var addNameItemValue = document.getElementById("addNameItem").value;
        var addProviderValue = document.getElementById("addNameProvider").value;
        var errorAddNameItem = document.getElementById("errorAddNameItem");
        var errorAddNameProvider = document.getElementById("errorAddNameProvider");

        if (addNameItemValue === "0") {
            errorAddNameItem.style.display = "block";
            if (addProviderValue === "0") {
                errorAddNameProvider.style.display = "block";
            }
            return false;
        } else if (addProviderValue === "0") {
            errorAddNameProvider.style.display = "block";
            return false;
        } else {
            return true;
        }
    }
    if (go === "update") {
        var inputNameItemValue = document.getElementById("inputNameItem").value;
        var inputProviderValue = document.getElementById("inputNameProvider").value;
        var errorInputNameItem = document.getElementById("errorInputNameItem");
        var errorInputNameProvider = document.getElementById("errorInputNameProvider");

        if (inputNameItemValue === "0") {
            errorInputNameItem.style.display = "block";
            if (inputProviderValue === "0" || addProviderValue === "0") {
                errorInputNameProvider.style.display = "block";
            }
            return false;
        } else if (inputProviderValue === "0") {
            errorInputNameProvider.style.display = "block";
            return false;
        } else {
            return true;
        }
    }

}

function validateSelect(selectId) {
    var selectValue = document.getElementById(selectId).value;
    var errorMessageElement = document.getElementById("error" + selectId.charAt(0).toUpperCase() + selectId.substring(1));

    if (selectValue !== "0") {
        errorMessageElement.style.display = "none";
    }
}

var searchInput = document.getElementById("searchInput");
var submitBnSearch = document.getElementById("submit-btn-search");
searchInput.addEventListener("keyup", function () {
    document.getElementById("search-results-container").innerHTML = "";
    SearchAll();
});

function submitSearch() {
    const searchInput = document.getElementById("searchInput").value;
    if (searchInput !== "") {
        window.location = "item-import?searchKey=" + searchInput;
    }
}

function changePage(page) {
    var searchKey = document.getElementById("searchKeyRequest").innerText;
    if (searchKey !== "") {
        window.location = "item-import?searchKey=" + searchKey + "&page=" + page;
    } else {
        window.location = "item-import?&page=" + page;
    }

}

searchInput.addEventListener("keydown", function (event) {
    if (event.key === "Enter") {
        submitBnSearch.click();
    }
});

function SearchAll() {
    console.log("Hello, World!");
    let data = {
        go: "search",
        searchKey: document.getElementById("searchInput").value,
        // page:
    }
    $.ajax({
        url: 'item-import-ajax',
        method: 'GET',
        data: data,
        success: function (response) {
            let searchList = document.getElementById("search-results-container");
            searchList.innerHTML
                = response;

        },
        error: function (xhr, status, error) {
            // Handle any errors
            console.log(error);
        }
    });
}

function showDropDown(id) {
    const showDropdownButton = document.getElementById('showDropdownButton' + id);
    const closeDropdownButton = document.getElementById('closeDropdownButton' + id);
    const dropdownContainer = document.getElementById('dropdownContainer' + id);
    if (dropdownContainer.style.display === 'none' || dropdownContainer.style.display === '') {
        dropdownContainer.style.display = 'block';
        showDropdownButton.style.display = 'none';
        closeDropdownButton.style.display = 'inherit';
    } else {
        dropdownContainer.style.display = 'none';
        showDropdownButton.style.display = 'inherit';
        closeDropdownButton.style.display = 'none';
    }
}
function datePicker() {
    datepicker();
}
